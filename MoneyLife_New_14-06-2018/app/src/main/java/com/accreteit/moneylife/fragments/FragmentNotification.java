package com.accreteit.moneylife.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.accreteit.moneylife.Config;
import com.accreteit.moneylife.LoginActivity;
import com.accreteit.moneylife.MainActivity;
import com.accreteit.moneylife.R;
import com.accreteit.moneylife.RetroApiClient;
import com.accreteit.moneylife.RetroApiInterface;
import com.accreteit.moneylife.adapters.NotificationAdapter;
import com.accreteit.moneylife.responses.Common;
import com.accreteit.moneylife.responses.GetAllNotifications;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentNotification extends Fragment {

    TextView toolbarTitle, noDataTv;
    ImageView backIv;
    RecyclerView recyclerView;
    NotificationAdapter adapter;
    GetAllNotifications an;
    ProgressDialog pd;
    // Static JSON
    // String notificationJson = "{ \"code\": 100, \"notification_list\": [ { \"id\": 1, \"name\": \"Khanak Sharma\", \"image\": \"http://www.gstatic.com/webp/gallery/1.jpg\", \"description\": \"She has replied to your question\", \"time\": \"10 mins ago\" }, { \"id\": 2, \"name\": \"Dev Dixit\", \"image\": \"http://www.gstatic.com/webp/gallery/2.jpg\", \"description\": \"He has cleared your doubt\", \"time\": \"5 mins ago\" }, { \"id\": 3, \"name\": \"Ananya Malwani\", \"image\": \"http://www.gstatic.com/webp/gallery/4.jpg\", \"description\": \"She has explained the answer with example\", \"time\": \"1 hour ago\" }, { \"id\": 4, \"name\": \"Misthy Shah\", \"image\": \"http://www.gstatic.com/webp/gallery/3.jpg\", \"description\": \"She has answered your question\", \"time\": \"15 mins ago\" } ] }";

    public FragmentNotification() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_notification, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        setHasOptionsMenu(true);

        initToolbar();

        noDataTv = rootView.findViewById(R.id.noDataTv);
        recyclerView = rootView.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (Config.isConnectedToInternet(getActivity())) {
            pd = new ProgressDialog(getActivity());
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();

            String userId = "", userType = "";
            userId = Config.getSharedPreferences(getActivity(), "userId");
            // userId = "2";  // Static user id for displaying data
            userType = Config.getSharedPreferences(getActivity(), "userType");
            Log.e("In FrgmtNotification", "Params : UserId -> " + userId + " n UserType -> " + userType);
            RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
            Call<GetAllNotifications> call = apiInterface.getAllNotifications(userId, userType);
            call.enqueue(new Callback<GetAllNotifications>() {
                @Override
                public void onResponse(Call<GetAllNotifications> call, Response<GetAllNotifications> response) {
                    pd.dismiss();
                    an = response.body();
                    Log.e("In FrgmtNotification", "Response : " + new Gson().toJson(an));
                    if (an.getCode() == 100) {
                        if (an.getNotificationList().size() > 0) {
                            noDataTv.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            adapter = new NotificationAdapter(getActivity(), an.getNotificationList());
                            recyclerView.setAdapter(adapter);
                        } else {
                            noDataTv.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }
                    } else {
                        Config.showDialog(getActivity(), Config.FailureMsg);
                    }
                }

                @Override
                public void onFailure(Call<GetAllNotifications> call, Throwable t) {
                    pd.dismiss();
                    Log.e("In FrgmtNotification", "OnFailure Excp : " + t.getMessage());
                    Config.showDialog(getActivity(), Config.FailureMsg);
                }
            });
        } else {
            Config.showAlertForInternet(getActivity());
        }

        return rootView;

    }

    public void initToolbar() {
        toolbarTitle = ((MainActivity) getActivity()).toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Notification");
        backIv = ((MainActivity) getActivity()).toolbar.findViewById(R.id.backIv);

        backIv.setImageResource(R.drawable.ic_toolbar_logo);
//        backIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getActivity().onBackPressed();
//            }
//        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
//        getActivity().getMenuInflater().inflate(R.menu.menu_home, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
//        if (itemId == R.id.action_search) {
//        } else {
//            getActivity().onBackPressed();
//        }
        return true;
    }

}
