package com.accreteit.moneylife.expert.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.wifi.WifiEnterpriseConfig;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.accreteit.moneylife.Config;
import com.accreteit.moneylife.R;
import com.accreteit.moneylife.RetroApiClient;
import com.accreteit.moneylife.RetroApiInterface;
import com.accreteit.moneylife.adapters.QuestionsAdapter;
import com.accreteit.moneylife.models.Expert;
import com.accreteit.moneylife.responses.GetQuestionsForExperts;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExpertDetail extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolbarTitle;
    ImageView backIv;
    TextView numOfAnswersTv, nameTv, categoryTv, descriptionTv, readMoreTv, noDataTv;
    ImageView imageIv;
    RecyclerView questionsRv;
    int expertId = 0;
    QuestionsAdapter adapter;
    ProgressDialog pd;
    Expert e;
//    String staticJson = "{ \"expert_id\": 1, \"num_of_answers\":26, \"expert_name\": \"Siddharth\", \"expert_image\": \"https://picsum.photos/458/354\", \"category\" : \"Government Exams\", \"description\": \"1234567890-wertyuiop[asdfghjkl;cxvvbnbm,.234567890-sdffgghhjkjlkqwertyuioopzxcvbnm,.234567890wertyuiopasdfghjkl;zxcvbnm,qwertyuioasdfghjkjlkzxcxvvbnnmm,qwertyuiopasdffgghhjjkklzxcvvbnm,123456778890qwertryuuiopasdfghjklzxcvvbnbnm,1234354567788990-0.,qwertyuioasdfghjkjlkzxcxvvbnnmm,qwertyuiopasdffgghhjjkklzxcvvbnm,123456778890qwertry,qwertyuioasdfghjkjlkzxcxvvbnnmm,qwertyuiopasdffgghhjjkklzxcvvbnm,123456778890qwertry,123456778890qwertryuuiopasdfghjklzxcvvbnbnm,1234354567788990-0.,\", \"questions\": [ { \"query_id\": 1, \"query_topic\": \"what is rti??\", \"query\": \"1234567890-wertyuiop[asdfghjkl;cxvvbnbm,.234567890-sdffgghhjkjlkqwertyuioopzxcvbnm,.234567890wertyuiopasdfghjkl;zxcvbnm,qwertyuioasdfghjkjlkzxcxvvbnnmm,qwertyuiopasdffgghhjjkklzxcvvbnm,123456778890qwertryuuiopasdfghjklzxcvvbnbnm,1234354567788990-0\", \"datetime\": \"11 mins ago\", \"username\": \"Misthy Shah\", \"user_image\": \"https://picsum.photos/458/354?gravity=east\", \"total_comments\":\"0\" }, { \"query_id\": 2, \"query_topic\": \"testing:-(\", \"query\": \"1234567890-wertyuiop[asdfghjkl;cxvvbnbm,.234567890-sdffgghhjkjlkqwertyuioopzxcvbnm,.234567890wertyuiopasdfghjkl;zxcvbnm,qwertyuioasdfghjkjlkzxcxvvbnnmm,qwertyuiopasdffgghhjjkklzxcvvbnm,123456778890qwertryuuiopasdfghjklzxcvvbnbnm,1234354567788990-0\", \"datetime\": \"05 mins ago\", \"username\": \"Dev Dixit\", \"user_image\": \"https://picsum.photos/458/354?gravity=east\", \"total_comments\":\"0\" }, { \"query_id\": 3, \"query_topic\": \"Have any query??\", \"query\": \"1234567890-wertyuiop[asdfghjkl;cxvvbnbm,.234567890-sdffgghhjkjlkqwertyuioopzxcvbnm,.234567890wertyuiopasdfghjkl;zxcvbnm,qwertyuioasdfghjkjlkzxcxvvbnnmm,qwertyuiopasdffgghhjjkklzxcvvbnm,123456778890qwertryuuiopasdfghjklzxcvvbnbnm,1234354567788990-0\", \"datetime\": \"1 hour ago\", \"username\": \"Ananya Sharma\", \"user_image\": \"https://picsum.photos/458/354?gravity=east\", \"total_comments\":\"0\" } ] }";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expert_detail);

        Intent intent = getIntent();
        if(intent != null){
            expertId = (Integer) intent.getSerializableExtra("expertId");
        }

        initToolbar();

        numOfAnswersTv = findViewById(R.id.numOfAnswersTv);
        nameTv = findViewById(R.id.nameTv);
        categoryTv = findViewById(R.id.categoryTv);
        descriptionTv = findViewById(R.id.descriptionTv);
        readMoreTv = findViewById(R.id.readMoreTv);
        noDataTv = findViewById(R.id.noDataTv);
        imageIv = findViewById(R.id.imageIv);
        questionsRv = findViewById(R.id.questionsRv);
        questionsRv.setLayoutManager(new LinearLayoutManager(ExpertDetail.this));

//        For static json
//        e = new Gson().fromJson(staticJson, Expert.class);

        if(Config.isConnectedToInternet(ExpertDetail.this)){
            pd = new ProgressDialog(ExpertDetail.this);
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.show();

            Log.e("In ExpertDetail", "Params : ExpertId -> " + expertId);
            RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
            Call<Expert> call = apiInterface.getExpertDetails(expertId + "");
            call.enqueue(new Callback<Expert>() {
                @Override
                public void onResponse(Call<Expert> call, Response<Expert> response) {
                    pd.dismiss();
                    Log.e("In ExpertDetail", "Response : " + new Gson().toJson(response.body()));;
                    e = response.body();
                    Glide.with(ExpertDetail.this)
                            .load(e.getImage())
                            .apply(new RequestOptions().circleCrop()
                                    .placeholder(R.drawable.image_loader)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true))
                            .into(imageIv);
                    numOfAnswersTv.setText(e.getNumOfAnswers() + " Answered");
                    nameTv.setText(e.getName());
                    categoryTv.setText(e.getCategory());
                    if(e.getDescription().length() > 150){
                        descriptionTv.setText(e.getDescription().substring(0, 150));
                        readMoreTv.setVisibility(View.VISIBLE);
                        readMoreTv.setText("Read More");
                    } else {
                        descriptionTv.setText(e.getDescription());
                        readMoreTv.setVisibility(View.GONE);
                    }

                    if(e.getQuestions().size() > 0){
                        noDataTv.setVisibility(View.GONE);
                        questionsRv.setVisibility(View.VISIBLE);
                        adapter = new QuestionsAdapter(ExpertDetail.this, e.getQuestions());
                        questionsRv.setAdapter(adapter);
                    } else {
                        noDataTv.setVisibility(View.VISIBLE);
                        questionsRv.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<Expert> call, Throwable t) {
                    pd.dismiss();
                    Log.e("In ExpertDetail", "OnFailure Excp : " + t.getMessage());
                }
            });
        } else {
            Config.showAlertForInternet(ExpertDetail.this);
        }

        readMoreTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(readMoreTv.getText().toString().equalsIgnoreCase("Read More")) {
                    descriptionTv.setText(e.getDescription());
                    readMoreTv.setText("Read Less");
                } else if(readMoreTv.getText().toString().equalsIgnoreCase("Read Less")){
                    descriptionTv.setText(e.getDescription().substring(0, 150));
                    readMoreTv.setText("Read More");
                }
            }
        });

    }

    public void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Expert Detail");
        backIv = toolbar.findViewById(R.id.backIv);

        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        onBackPressed();
//        return true;
//    }
}
