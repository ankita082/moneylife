package com.accreteit.moneylife.responses;

import com.accreteit.moneylife.models.Expert;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 22-Mar-18.
 */

public class GetExperts {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("expert")
    @Expose
    private List<Expert> expertList = null;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<Expert> getExpertList() {
        return expertList;
    }

    public void setExpertList(List<Expert> expertList) {
        this.expertList = expertList;
    }
}
