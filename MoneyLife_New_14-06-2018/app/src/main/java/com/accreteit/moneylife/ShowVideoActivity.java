package com.accreteit.moneylife;

import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

public class ShowVideoActivity extends AppCompatActivity {

    VideoView videoView;
    String url = "";
    Toolbar toolbar;
    TextView toolbarTitle;
    ImageView backIv;
    static int errorCount = 0;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_video);

        initToolbar();

        videoView = findViewById(R.id.videoView);
        url = getIntent().getStringExtra("url");
        Log.e("In ShowVideoAct", "Url : " + url);

        videoView = findViewById(R.id.videoView);

        pd = new ProgressDialog(ShowVideoActivity.this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();

        MediaController mediaController = new MediaController(ShowVideoActivity.this);
        videoView.setMediaController(mediaController);
        mediaController.setAnchorView(videoView);
        videoView.setVideoURI(Uri.parse(url));

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                pd.dismiss();
                videoView.seekTo(500);
//                videoView.start();
            }
        });

        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                Log.e("In ShowVideoAct", "In OnError");
                if(pd.isShowing())
                    pd.dismiss();
                videoView.suspend();
                // Config.showDialog(ShowVideoActivity.this, Config.FailureMsg);
                return false;
            }
        });

    }

    public void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Video");
        backIv = toolbar.findViewById(R.id.backIv);

        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoView.pause();
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        onBackPressed();
//        return true;
//    }
}
