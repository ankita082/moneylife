package com.accreteit.moneylife;


import com.accreteit.moneylife.models.Expert;
import com.accreteit.moneylife.models.Question;
import com.accreteit.moneylife.responses.AddComment;
import com.accreteit.moneylife.responses.Common;
import com.accreteit.moneylife.responses.GetAllCategories;
import com.accreteit.moneylife.responses.GetAllNotifications;
import com.accreteit.moneylife.responses.GetExperts;
import com.accreteit.moneylife.responses.GetQuestions;
import com.accreteit.moneylife.responses.GetQuestionsForExperts;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RetroApiInterface {
    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @POST("login.html")
    public Call<Common> loginUsingServer(@Query("email") String email, @Query("password") String password,
                                         @Query("device_token") String deviceToken);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @POST("signup.html")
    public Call<Common> registerUsingServer(@Query("name") String name, @Query("email") String email,
                                            @Query("password") String password, @Query("device_type") String deviceType,
                                            @Query("account_type") String registerType,
                                            @Query("device_token") String deviceToken);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @POST("signup.html")
    public Call<Common> registerFacebook(@Query("email") String email, @Query("account_type") String registerType,
                                         @Query("facebook_uid") String facebookId, @Query("name") String name,
                                         @Query("device_token") String deviceToken);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @POST("signup.html")
    public Call<Common> registerGoogle(@Query("email") String email, @Query("account_type") String registerType,
                                       @Query("google_uid") String gPlusId, @Query("name") String gPlusName,
                                       @Query("device_token") String deviceToken);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @POST("forgotpwd.html")
    public Call<Common> forgotPassword(@Query("email") String email);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @Multipart
    @POST("profileupdate.html")
    public Call<Common> updateProfileWithMultipart(@Query("email") String email, @Query("userid") String userid,
                                                   @Query("user_name") String username,
                                                   @Query("full_name") String full_name,
                                                   @Query("mobile_no") String mobile_no,
                                                   @Part MultipartBody.Part image);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @POST("profileupdate.html")
    public Call<Common> updateProfileWithoutMultipart(@Query("email") String email, @Query("userid") String userid,
                                                      @Query("user_name") String username,
                                                      @Query("full_name") String full_name,
                                                      @Query("mobile_no") String mobile_no);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @POST("profileinfo.html")
    public Call<Common> getProfile(@Query("email") String email, @Query("userid") String userid);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @POST("changepassword.html")
    public Call<Common> changePassword(@Query("email") String email, @Query("userid") String userid,
                                       @Query("oldpassword") String oldPassword, @Query("newpassword") String newPassword);

    @FormUrlEncoded
//    @POST("login.php")
    @POST("login")
    public Call<Common> loginForExpert(@Field("email") String email, @Field("password") String password,
                                       @Field("device_token") String deviceToken, @Field("device_type") String deviceType);

    @FormUrlEncoded
//    @POST("login.php")
    @POST("getexpertprofile")
    public Call<Common> getProfileForExpert(@Field("expert_id") String expertId);

    @FormUrlEncoded
//    @POST("login.php")
    @POST("changeexpertpassword")
    public Call<Common> changePasswordForExpert(@Field("expert_id") String expertId, @Field("oldpassword") String oldPassword,
                                                @Field("newpassword") String newPassword);

    @Multipart
    @POST("updateexpertprofile")
    public Call<Common> updateProfileWithMultipartForExpert(@Part("expert_id") RequestBody expertId,
                                                            @Part("expert_email") RequestBody expertEmail,
                                                            @Part("expert_name") RequestBody expertName,
                                                            @Part MultipartBody.Part image);

    @Multipart
    @POST("updateexpertprofile")
    public Call<Common> updateProfileWithoutMultipartForExpert(@Part("expert_id") RequestBody expertId,
                                                               @Part("expert_email") RequestBody expertEmail,
                                                               @Part("expert_name") RequestBody expertName);

    @POST("signup.php")
    public Call<Common> registerUsingDb(@Query("firstName") String firstName, @Query("email") String email,
                                        @Query("password") String password, @Query("mobile_no") String mobileNumber,
                                        @Query("device_id") String deviceId, @Query("device_type") String deviceType);

    @FormUrlEncoded
    @POST("adduser")
    public Call<Common> addUserToDb(@Field("email") String email, @Field("username") String userName,
                                    @Field("user_image") String userImage, @Field("user_id") String userIdFromServer,
                                    @Field("device_id") String deviceId, @Field("device_type") String deviceType);

    @Multipart
//    @POST("ask_query.php")
    @POST("ask_query")
    public Call<Common> askQuery(@Part("query_topic") RequestBody queryTopic, @Part("query") RequestBody query,
                                 @Part("user_id") RequestBody user_id, @Part("expert_id") RequestBody expert_id,
                                 @Part MultipartBody.Part video, @Part List<MultipartBody.Part> attachments,
                                 @Part("total_attachments") RequestBody total_attachments);

    @FormUrlEncoded
//    @POST("ask_query.php")
    @POST("ask_query")
    public Call<Common> askQueryWithoutMultipart(@Field("query_topic") String queryTopic, @Field("query") String query,
                                                 @Field("user_id") String user_id, @Field("expert_id") String expert_id);

    @Multipart
//    @POST("answer_the_question.php")
    @POST("answer_the_question")
    public Call<Common> answerQuery(@Part("query_id") RequestBody queryId, @Part("expert_id") RequestBody expert_id,
                                    @Part("answer_topic") RequestBody replyTitle, @Part("answer") RequestBody reply,
                                    @Part MultipartBody.Part video, @Part List<MultipartBody.Part> attachments,
                                    @Part("total_attachments") RequestBody total_attachments);

    @FormUrlEncoded
//    @POST("answer_the_question.php")
    @POST("answer_the_question")
    public Call<Common> answerQueryWithoutMultipart(@Field("query_id") String queryId, @Field("expert_id") String expert_id,
                                                    @Field("answer_topic") String replyTitle, @Field("answer") String reply);

    //    @POST("list_category.php")
    @POST("list_category")
    public Call<GetAllCategories> getCategories();

    @FormUrlEncoded
//    @POST("list_notification.php")
    @POST("list_notification")
    public Call<GetAllNotifications> getAllNotifications(@Field("user_id") String userId, @Field("user_type") String userType);

    //    @POST("list_expert.php")
    @POST("list_expert")
    public Call<GetExperts> getAllExperts();

    @FormUrlEncoded
//    @POST("list_question.php")
    @POST("list_question")
    public Call<GetQuestions> getAllQuestions(@Field("category_id") String categoryId);

    @FormUrlEncoded
//    @POST("list_user_question.php")
    @POST("list_user_question")
    public Call<GetQuestions> getUsersQuestions(@Field("user_id") String userId);

//    @FormUrlEncoded
//    @POST("change_password.php")
//    public Call<Common> changePassword(@Field("user_id") String userId, @Field("old_password") String oldPassword, @Field("new_password") String newPassword);

    @FormUrlEncoded
//    @POST("single_question.php")
    @POST("single_query")
    public Call<Question> getQuestionDetails(@Field("query_id") String queryId);

    @FormUrlEncoded
//    @POST("expert_details.php")
    @POST("expert_details")
    public Call<Expert> getExpertDetails(@Field("expert_id") String expertId);

    @FormUrlEncoded
//    @POST("expert_request_open.php")
    @POST("expert_request_open")
    public Call<GetQuestionsForExperts> getUnansweredQuestions(@Field("expert_id") String expertId);

    @FormUrlEncoded
//    @POST("expert_request_answered.php")
    @POST("expert_request_answered")
    public Call<GetQuestionsForExperts> getAnsweredQuestions(@Field("expert_id") String expertId);

    @FormUrlEncoded
//    @POST("add_comment.php")
    @POST("addcomment")
    public Call<AddComment> addComment(@Field("user_id") String userId, @Field("user_type") String userType,
                                       @Field("query_id") String queryId, @Field("comment") String comment);
}
