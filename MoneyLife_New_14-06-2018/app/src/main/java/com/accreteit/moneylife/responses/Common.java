package com.accreteit.moneylife.responses;

import com.accreteit.moneylife.models.Category;
import com.accreteit.moneylife.models.UserInfo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Common {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message = "";
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("UserId")
    @Expose
    private String userIdFromServer;
    @SerializedName("is_user")
    @Expose
    private Integer isUser;
    @SerializedName("isPremiem")
    @Expose
    private Integer isPremium;
    @SerializedName("user_info")
    @Expose
    private UserInfo userInfo;

    // Note :- For Expert
    @SerializedName("expert_id")
    @Expose
    private Integer expertId;
    @SerializedName("expert_email")
    @Expose
    private String expertEmail;
    @SerializedName("expert_name")
    @Expose
    private String expertName;
    @SerializedName("expert_image")
    @Expose
    private String expertImage;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("description")
    @Expose
    private String description;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getIsUser() {
        return isUser;
    }

    public void setIsUser(Integer isUser) {
        this.isUser = isUser;
    }

    public String getUserIdFromServer() {
        return userIdFromServer;
    }

    public Integer getIsPremium() {
        return isPremium;
    }

    public void setIsPremium(Integer isPremium) {
        this.isPremium = isPremium;
    }

    public void setUserIdFromServer(String userIdFromServer) {
        this.userIdFromServer = userIdFromServer;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public Integer getExpertId() {
        return expertId;
    }

    public void setExpertId(Integer expertId) {
        this.expertId = expertId;
    }

    public String getExpertEmail() {
        return expertEmail;
    }

    public void setExpertEmail(String expertEmail) {
        this.expertEmail = expertEmail;
    }

    public String getExpertName() {
        return expertName;
    }

    public void setExpertName(String expertName) {
        this.expertName = expertName;
    }

    public String getExpertImage() {
        return expertImage;
    }

    public void setExpertImage(String expertImage) {
        this.expertImage = expertImage;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
