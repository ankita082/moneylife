package com.accreteit.moneylife;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class Config {
//    public final static String URL = "http://www.accreteit.com/moneylife_app/api/";
//    public final static String URL = "http://192.168.3.118:81/moneylife/api/";
    public final static String URL = "http://accreteit.com/rticentre/api/";
    //    public final static String URL = "http://www.tweetresorts.com/bitcoinexchange/api/";
    public final static String FailureMsg = "Oops!! Something went wrong, please try again...";
    public final static String SignUpnLoginUrl = "http://demo.auth.moneylife.in/api/";


    public static void showSnackBar(Activity activity, String message) {
        Snackbar snackbar = Snackbar
                .make(activity.findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.parseColor("#444444"));
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.parseColor("#eeeeee"));
        textView.setTextSize(15);
        snackbar.show();
    }

    public static void saveSharedPreferences(Context context, String key,
                                             String value) {
        SharedPreferences pref = context.getSharedPreferences("MyPref",
                Context.MODE_PRIVATE);
        if (pref != null) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(key, value);
            editor.commit();
        }
    }

    public static String getSharedPreferences(Context context, String key) {
        SharedPreferences pref = context.getSharedPreferences("MyPref",
                Context.MODE_PRIVATE);
        return pref.getString(key, null);
    }

    public static boolean isConnectedToInternet(Context _context) {
        ConnectivityManager connectivity = (ConnectivityManager) _context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public static void showAlertForInternet(final Activity context) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("No Internet Connection");
        alertDialog.setMessage("Please make sure that you are connected to the internet");
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.fail);

        alertDialog.setButton("Go to Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                context.startActivity(intent);
                context.finish();
            }
        });
        alertDialog.show();
    }

//    public static JSONArray shuffleArray(JSONArray array) {
//        JSONArray sliderArray = null;
//        try {
//            sliderArray = new JSONArray();
//            Random rnd = new Random();
//            for (int i = array.length() - 1; i > 0; i--) {
//                int index = rnd.nextInt(i + 1);
//                // Simple swap
//                JSONObject sliderRow = array.getJSONObject(index);
//                sliderArray.put(sliderRow);
//            }
//        } catch (JSONException je) {
//            Log.e("In Config", "Shuffle Slider : " + je.getMessage());
//        }
//        return sliderArray;
//    }

    public static void hideSoftKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
        View view = ((Activity) context).getCurrentFocus();
        if (view != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showDialog(Context context, String message) {
        final DialogPlus dialog = DialogPlus.newDialog(context)
                .setContentHolder(new ViewHolder(R.layout.dialog_ok_layout))
                .setContentHeight(LinearLayout.LayoutParams.WRAP_CONTENT)
                .setGravity(Gravity.CENTER)
                .create();
        dialog.show();

        View errorView = dialog.getHolderView();
        TextView titleTv = (TextView) errorView.findViewById(R.id.titleTv);
        TextView messageTv = (TextView) errorView.findViewById(R.id.messageTv);
        Button okBtn = (Button) errorView.findViewById(R.id.okBtn);
        titleTv.setVisibility(View.GONE);
        messageTv.setText(message);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }


}
