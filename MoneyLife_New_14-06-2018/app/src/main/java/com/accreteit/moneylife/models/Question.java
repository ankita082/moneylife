package com.accreteit.moneylife.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Question {
    @SerializedName("query_id")
    @Expose
    private Integer questionId;
    @SerializedName("query_topic")
    @Expose
    private String questionTitle;
    @SerializedName("query_video")
    @Expose
    private String questionVideo;
    @SerializedName("query")
    @Expose
    private String questionDescription;
    @SerializedName("datetime")
    @Expose
    private String dateTime;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String userName;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("expert_id")
    @Expose
    private int expertId;
    @SerializedName("expert_name")
    @Expose
    private String expertName;
    @SerializedName("expert_image")
    @Expose
    private String expertImage;
    @SerializedName("is_answered")
    @Expose
    private int isAnswered;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("answer_topic")
    @Expose
    private String answerTitle;
    @SerializedName("answer_id")
    @Expose
    private String answerId;
    @SerializedName("answer_datetime")
    @Expose
    private String answerDateTime;
    @SerializedName("total_comments")
    @Expose
    private String commentsNumber;
    @SerializedName("query_medias")
    @Expose
    private List<Media> queryMediaList;
    @SerializedName("answer_medias")
    @Expose
    private List<Media> answerMediaList;
    @SerializedName("comments")
    @Expose
    private List<Comment> commentsList;
    @SerializedName("answer_video")
    @Expose
    private String answerVideo;

    public List<Media> getQueryMediaList() {
        return queryMediaList;
    }

    public void setQueryMediaList(List<Media> queryMediaList) {
        this.queryMediaList = queryMediaList;
    }

    public List<Comment> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<Comment> commentsList) {
        this.commentsList = commentsList;
    }

    public List<Media> getAnswerMediaList() {
        return answerMediaList;
    }

    public void setAnswerMediaList(List<Media> answerMediaList) {
        this.answerMediaList = answerMediaList;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public String getQuestionDescription() {
        return questionDescription;
    }

    public void setQuestionDescription(String questionDescription) {
        this.questionDescription = questionDescription;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public int getExpertId() {
        return expertId;
    }

    public void setExpertId(int expertId) {
        this.expertId = expertId;
    }

    public String getExpertName() {
        return expertName;
    }

    public void setExpertName(String expertName) {
        this.expertName = expertName;
    }

    public String getExpertImage() {
        return expertImage;
    }

    public void setExpertImage(String expertImage) {
        this.expertImage = expertImage;
    }

    public int getIsAnswered() {
        return isAnswered;
    }

    public void setIsAnswered(int isAnswered) {
        this.isAnswered = isAnswered;
    }

    public String getAnswerTitle() {
        return answerTitle;
    }

    public void setAnswerTitle(String answerTitle) {
        this.answerTitle = answerTitle;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getAnswerDateTime() {
        return answerDateTime;
    }

    public void setAnswerDateTime(String answerDateTime) {
        this.answerDateTime = answerDateTime;
    }

    public String getCommentsNumber() {
        return commentsNumber;
    }

    public void setCommentsNumber(String commentsNumber) {
        this.commentsNumber = commentsNumber;
    }

    public String getQuestionVideo() {
        return questionVideo;
    }

    public void setQuestionVideo(String questionVideo) {
        this.questionVideo = questionVideo;
    }

    public String getAnswerVideo() {
        return answerVideo;
    }

    public void setAnswerVideo(String answerVideo) {
        this.answerVideo = answerVideo;
    }


}
