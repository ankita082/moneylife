package com.accreteit.moneylife.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.accreteit.moneylife.Config;
import com.accreteit.moneylife.GridSpacingItemDecoration;
import com.accreteit.moneylife.MainActivity;
import com.accreteit.moneylife.R;
import com.accreteit.moneylife.RetroApiClient;
import com.accreteit.moneylife.RetroApiInterface;
import com.accreteit.moneylife.adapters.CategoryAdapter;
import com.accreteit.moneylife.adapters.ExpertAdapter;
import com.accreteit.moneylife.models.Expert;
import com.accreteit.moneylife.responses.GetAllCategories;
import com.accreteit.moneylife.responses.GetExperts;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentExperts extends Fragment {

    TextView toolbarTitle;
    ImageView backIv;
    RecyclerView recyclerView;
    TextView noDataTv;
    ProgressDialog pd;
    ExpertAdapter adapter;
//    String expertJson = "{ \"code\": 100, \"expert_list\": [ { \"id\": 1, \"name\": \"Misthy Shah\", \"image\": \"http://www.gstatic.com/webp/gallery/1.jpg\" }, { \"id\": 2, \"name\": \"Gunjan Malhotra\", \"image\": \"http://www.gstatic.com/webp/gallery/2.jpg\" }, { \"id\": 3, \"name\": \"Ananya Malwani\", \"image\": \"http://www.gstatic.com/webp/gallery/4.jpg\" }, { \"id\": 4, \"name\": \"Khanak Sharma\", \"image\": \"http://www.gstatic.com/webp/gallery/3.jpg\" }, { \"id\": 5, \"name\": \"Dev Dixit\", \"image\": \"http://www.gstatic.com/webp/gallery/5.jpg\" } ] }";
//    GetExperts e;

    public FragmentExperts() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_experts, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        setHasOptionsMenu(true);

        initToolbar();

        recyclerView = rootView.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        noDataTv = rootView.findViewById(R.id.noDataTv);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, 30, true));

        if (Config.isConnectedToInternet(getActivity())) {
            pd = new ProgressDialog(getActivity());
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();

            RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
            Call<GetExperts> call = apiInterface.getAllExperts();
            call.enqueue(new Callback<GetExperts>() {
                @Override
                public void onResponse(Call<GetExperts> call, Response<GetExperts> response) {
                    pd.dismiss();
                    GetExperts ac = response.body();
                    if (ac.getExpertList().size() == 0) {
                        noDataTv.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    } else {
                        noDataTv.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        adapter = new ExpertAdapter(getActivity(), ac.getExpertList());
                        recyclerView.setAdapter(adapter);
                    }
                }

                @Override
                public void onFailure(Call<GetExperts> call, Throwable t) {
                    pd.dismiss();
                    Config.showDialog(getActivity(), Config.FailureMsg);
                }
            });
        } else {
            Config.showAlertForInternet(getActivity());
        }
        return rootView;
    }

    public void initToolbar() {
        toolbarTitle = ((MainActivity) getActivity()).toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Experts");
        backIv = ((MainActivity) getActivity()).toolbar.findViewById(R.id.backIv);

        backIv.setImageResource(R.drawable.ic_toolbar_logo);
//        backIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getActivity().onBackPressed();
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        getActivity().onBackPressed();
        return true;
    }

}
