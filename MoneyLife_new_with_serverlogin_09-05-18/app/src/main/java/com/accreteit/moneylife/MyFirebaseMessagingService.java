package com.accreteit.moneylife;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HOME on 13-Sep-16.
 */
public class MyFirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    private Context context = this;
    int isForeground = 0;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e("In FirebaseMsgService", "In OnMsgReceived");
        if (remoteMessage.getData().size() > 0) {
            showNotification(remoteMessage.getData().get("Message"));
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            String data = remoteMessage.getNotification().getBody();
            showNotification(data);
        }
    }

    private void showNotification(String messageBody) {
        String title = "", message = "";
        Log.e("In FirebaseMsgService", "In SendNotification");
        int code = 101;

        try {
            JSONObject jsonObject = new JSONObject(messageBody);
            code = jsonObject.getInt("code");
            title = jsonObject.getString("notification_title");
            message = jsonObject.getString("notification_message");

        } catch (JSONException e) {
            Log.e("Exception", "Exception : " + e.getMessage());
        }
        Intent intent = null;
//        try {
//            if (code == 116 || code == 118) {
//                    ActivityManager activityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
//                    List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
//                    for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
//                        if (getApplicationContext().getPackageName().equals(appProcess.processName)) {
//                            Log.e("In If ", "PackageName Same : " + appProcess.processName);
//                            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
//                                Log.e("Foreground App", appProcess.processName);
//                                isForeground = 1;
////                            Handler handler = new Handler(Looper.getMainLooper());
////                            handler.postDelayed(new Runnable() {
////                                @Override
////                                public void run() {
////                                    AlertDialog dialog = new AlertDialog.Builder(new ContextThemeWrapper(getApplicationContext(), R.style.AppTheme))
////                                            .setTitle("Notification")
////                                            .setMessage(finalMessage)
////                                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
////                                                @Override
////                                                public void onClick(DialogInterface dialog, int which) {
////                                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "911"));
////                                                    startActivity(intent);
////                                                }
////                                            })
////                                            .show();
////                                    dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//                                Intent intent1 = new Intent(context, NotificationDialogActivity.class);
//                                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                intent1.putExtra("message", message);
//                                startActivity(intent1);
////                                }
////                            }, 1000);
//                                // dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//
//                            } else {
//                                Log.e("Background App", appProcess.processName);
//                                isForeground = 0;
//                            }
//                        }
//                    }
//            }
//        } catch (SecurityException e) {
//            Log.e("In FirebaseMsgService", "Excp Msg : " + e.getMessage());
//        }
        intent = new Intent(this, MainActivity.class);

        Log.e("In FirebaseMsgService", "Message Value is : " + message);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(MyFirebaseMessagingService.this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        if (isForeground == 0) {
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            android.support.v4.app.NotificationCompat.Builder notificationBuilder = new android.support.v4.app.NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setColor(Color.parseColor("#ffffff"))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }
    }

//    private int getNotificationIcon(android.support.v4.app.NotificationCompat.Builder notificationBuilder) {
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            return R.mipmap.ic_launcher;
//        } else {
//            return R.mipmap.ic_launcher;
//        }
//    }


}


