package com.accreteit.moneylife;

import android.app.ProgressDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.accreteit.moneylife.responses.Common;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.accreteit.moneylife.RetroApiClient.okHttpClient;

public class ForgotPassword extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolbarTitle;
    ImageView backIv;
    TextInputLayout emailInput;
    EditText emailEt;
    TextView forgotPwdBtn;
    String email = "";
    Boolean isValid;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        initToolbar();

        emailInput = findViewById(R.id.emailInput);
        emailEt = findViewById(R.id.emailEt);
        forgotPwdBtn = findViewById(R.id.forgotPwdBtn);
        
        forgotPwdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isValid = true;
                email = emailEt.getText().toString();
                emailInput.setErrorEnabled(false);
                final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if (email.equalsIgnoreCase("")) {
                    isValid = false;
                    emailInput.setErrorEnabled(true);
                    emailInput.setError("Must enter email address");
                } else {
                    if (!email.matches(emailPattern)) {
                        isValid = false;
                        emailInput.setErrorEnabled(true);
                        emailInput.setError("Must enter valid email id");
                    }
                }

                if(isValid) {
                    if (Config.isConnectedToInternet(ForgotPassword.this)) {
                        pd = new ProgressDialog(ForgotPassword.this);
                        pd.setTitle("Please wait");
                        pd.setMessage("Loading..");
                        pd.setCancelable(false);
                        pd.show();
                        Retrofit retro = new Retrofit.Builder()
                                .baseUrl(Config.SignUpnLoginUrl)
                                .addConverterFactory(GsonConverterFactory.create())
                                .client(okHttpClient)
                                .build();
                        String email = Config.getSharedPreferences(ForgotPassword.this, "email");
                        Log.e("In ForgotPassword", "Params : Email -> " + email);
                        RetroApiInterface apiInterface = retro.create(RetroApiInterface.class);
                        Call<Common> call = apiInterface.forgotPassword(email);
                        call.enqueue(new Callback<Common>() {
                            @Override
                            public void onResponse(Call<Common> call, Response<Common> response) {
                                pd.dismiss();
                                Common c = response.body();
                                if (c != null && c.getCode() == 100) {
                                    Config.showDialog(ForgotPassword.this, c.getMessage());
                                } else {
                                    Config.showDialog(ForgotPassword.this, c.getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<Common> call, Throwable t) {
                                pd.dismiss();
                                Log.e("In ForgotPassword", "In OnFailure Msg : " + t.getMessage());
                                Config.showDialog(ForgotPassword.this, Config.FailureMsg);
                            }
                        });

                    } else {
                        Config.showAlertForInternet(ForgotPassword.this);
                    }
                }
            }
        });

    }

    public void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Forgot Password");
        backIv = toolbar.findViewById(R.id.backIv);

        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

}
