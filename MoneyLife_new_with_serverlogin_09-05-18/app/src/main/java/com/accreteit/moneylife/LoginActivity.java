package com.accreteit.moneylife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.accreteit.moneylife.models.UserInfo;
import com.accreteit.moneylife.responses.Common;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.accreteit.moneylife.RetroApiClient.okHttpClient;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    EditText emailEt, passwordEt;
    TextInputLayout emailInput, passwordInput;
    TextView loginBtn, fbBtn, gPlusBtn, forgotPwdTv, registerTv;
    LinearLayout fbLayout, gPlusLayout;
    String fbId = "", gPlusId = "", name = "", email = "", password = "", deviceType = "0", token = "", registerType = "";
    private static final int RC_SIGN_IN = 100;
    CallbackManager callbackManager;
    GoogleApiClient mGoogleApiClient;
    GoogleSignInOptions gso;
    ProgressDialog pd;
    Boolean isValid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailInput = findViewById(R.id.emailInput);
        passwordInput = findViewById(R.id.passwordInput);
        emailEt = findViewById(R.id.emailEt);
        passwordEt = findViewById(R.id.passwordEt);
        fbLayout = findViewById(R.id.fbLayout);
        gPlusLayout = findViewById(R.id.gPlusLayout);
        loginBtn = findViewById(R.id.loginBtn);
        fbBtn = findViewById(R.id.fbBtn);
        gPlusBtn = findViewById(R.id.gPlusBtn);
        forgotPwdTv = findViewById(R.id.forgotPwdTv);
        registerTv = findViewById(R.id.registerTv);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e("In LoginActivity", "Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("In LoginActivity", "Excp : " + e.getMessage());
        } catch (Exception e) {
            Log.e("In LoginActivity", "Excp : " + e.getMessage());
        }

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Log.e("In LoginActivity", "In OnSuccess");

                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    fbId = response.getJSONObject().getString("id");
                                    name = response.getJSONObject().getString("name");
                                    email = response.getJSONObject().getString("email");
                                    registerApiCall("1");
                                } catch (JSONException je) {
                                    Log.e("In LoginActivity", "Graph JSON Excp : " + je.getMessage());
                                }
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.e("In LoginActivity", "In OnCancel");
                        Toast.makeText(LoginActivity.this, "Login Cancelled!", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.e("In LoginActivity", "In OnError");
                        Toast.makeText(LoginActivity.this, Config.FailureMsg, Toast.LENGTH_LONG).show();
                    }
                });

//       Note :- Values for registerType --> 0 -> For normal app registration, 1 -> For Fb and 2 -> For Google

        if (Config.getSharedPreferences(LoginActivity.this, "token") != null) {
            token = Config.getSharedPreferences(LoginActivity.this, "token");
        } else {
            token = FirebaseInstanceId.getInstance().getToken();
        }
        Config.saveSharedPreferences(LoginActivity.this, "token", token);

        /*  // Custom Data
        emailEt.setText("shashankbansal21@gmail.com");
        passwordEt.setText("123456");  */

        forgotPwdTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgotPassword.class);
                startActivity(intent);
            }
        });

        registerTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        fbLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerType = "1";
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this,
                        Arrays.asList("email", "public_profile"));
            }
        });

        gPlusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerType = "2";

                gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .requestProfile()
                        .requestId()
                        .build();

                if (mGoogleApiClient != null) {
                    Log.e("In LoginActivity", "In IF");
                    OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
                    if (opr.isDone()) {
                        Log.e("In LoginActivity", "In Inner IF");
                        // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
                        // and the GoogleSignInResult will be available instantly.
                        Log.e("In OnStart", "Got cached sign-in");
                        GoogleSignInResult result = opr.get();
                        handleSignInResult(result);
                    } else {
                        Log.e("In LoginActivity", "In Inner Else");
                        // If the user has not previously signed in on this device or the sign-in has expired,
                        // this asynchronous branch will attempt to sign in the user silently.  Cross-device
                        // single sign-on will occur in this branch.
                        mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
//                            .enableAutoManage(LoginActivity.this, LoginActivity.this)
                                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                                .build();

                        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                        startActivityForResult(signInIntent, RC_SIGN_IN);
                    }
                } else {
                    Log.e("In LoginActivity", "In Else");
                    mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
                            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                            .build();

                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                }

            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isValid = true;
                email = emailEt.getText().toString();
                password = passwordEt.getText().toString();
                emailInput.setErrorEnabled(false);
                passwordInput.setErrorEnabled(false);
                final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if (email.equalsIgnoreCase("")) {
                    isValid = false;
                    emailInput.setErrorEnabled(true);
                    emailInput.setError("Must enter email address");
                } else {
                    if (!email.matches(emailPattern)) {
                        isValid = false;
                        emailInput.setErrorEnabled(true);
                        emailInput.setError("Must enter valid email id");
                    }
                }
                if (password.equalsIgnoreCase("")) {
                    isValid = false;
                    passwordInput.setErrorEnabled(true);
                    passwordInput.setError("Must enter password");
                }

                if (isValid) {
                    if (Config.isConnectedToInternet(LoginActivity.this)) {
                        pd = new ProgressDialog(LoginActivity.this);
                        pd.setTitle("Please wait");
                        pd.setMessage("Loading...");
                        pd.setCancelable(false);
                        pd.show();

                        Log.e("In LoginActivity", "Params : Email -> " + email + " Pwd -> " + password
                                + " DeviceType -> " + deviceType + " n DeviceToken -> " + token);
                        Retrofit retro = new Retrofit.Builder()
                                .baseUrl(Config.SignUpnLoginUrl)
                                .addConverterFactory(GsonConverterFactory.create())
                                .client(okHttpClient)
                                .build();
                        RetroApiInterface apiInterface = retro.create(RetroApiInterface.class);
//                        Call<Common> call = apiInterface.loginUsingServer(email, password, deviceType, token);
                        Call<Common> call = apiInterface.loginUsingServer(email, password, token);
                        call.enqueue(new Callback<Common>() {
                            @Override
                            public void onResponse(Call<Common> call, Response<Common> response) {
//                                pd.dismiss();
                                Common c = response.body();
                                Log.e("In LoginActivity", "Response : " + response.body());
                                if (c != null && c.getCode() == 100) {
//                                    pd.dismiss();
                                    Log.e("In LoginActivity", "In IF Actual Server");
                                    // Custom Data
                                    Config.saveSharedPreferences(LoginActivity.this, "userId", c.getUserIdFromServer());
                                    Config.saveSharedPreferences(LoginActivity.this, "email", email);
                                    Config.saveSharedPreferences(LoginActivity.this, "userType", "1");
                                    finish();
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else {
                                    Log.e("In LoginActivity", "In Else Db");
                                    RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
                                    Call<Common> call1 = apiInterface.loginUsingDb(email, password, token, "0");
                                    call1.enqueue(new Callback<Common>() {
                                        @Override
                                        public void onResponse(Call<Common> call, Response<Common> response) {
                                            pd.dismiss();
                                            Common c = response.body();
                                            Config.saveSharedPreferences(LoginActivity.this, "userId", c.getUserId());
                                            Config.saveSharedPreferences(LoginActivity.this, "email", email);
                                            Config.saveSharedPreferences(LoginActivity.this, "userType", c.getIsUser() + "");
                                            finish();
                                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }

                                        @Override
                                        public void onFailure(Call<Common> call, Throwable t) {
                                            pd.dismiss();
                                            Config.showDialog(LoginActivity.this, Config.FailureMsg);
                                        }
                                    });

                                }
                            }

                            @Override
                            public void onFailure(Call<Common> call, Throwable t) {
                                pd.dismiss();
                                Log.e("In LoginActivity", "OnFailure Excp : " + t.getMessage());
                                Config.showDialog(LoginActivity.this, Config.FailureMsg);
                            }
                        });
                    } else {
                        Config.showAlertForInternet(LoginActivity.this);
                    }
                }

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("In LoginActivity", "In OnConnFailed" + connectionResult);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.e("In LoginActivity", "In handleSignInResult : " + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            Log.e("In HandleSignIn", "Success : " + result.isSuccess());
            GoogleSignInAccount acct = result.getSignInAccount();
            /*String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();
            String id = acct.getId();*/

            Log.e("In HandleSignIn", "Name: " + acct.getDisplayName() + ", email: " + acct.getEmail()
                    + ", Image: " + acct.getPhotoUrl() + " n Id: " + acct.getId());
//            nameEt.setText(acct.getDisplayName());
//            emailEt.setText(acct.getEmail());
            name = acct.getDisplayName();
            email = acct.getEmail();
            gPlusId = acct.getId();
            registerApiCall("2");

        } else {
            // Signed out, show unauthenticated UI.
            Log.e("In HandleSignIn", "In Failure");
        }
    }


    public void registerApiCall(final String registerType) {
        if (Config.isConnectedToInternet(LoginActivity.this)) {
            pd = new ProgressDialog(LoginActivity.this);
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();

            Log.e("In LoginActivity", "Params : Name -> " + name + " Email -> " + email
                    + " Pwd -> " + password + " DeviceType -> " + deviceType
                    + " RegisterType -> " + registerType + " FbId -> " + fbId + " gPlusId -> " + gPlusId);
            Retrofit retro = new Retrofit.Builder()
                    .baseUrl(Config.SignUpnLoginUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            RetroApiInterface apiInterface = retro.create(RetroApiInterface.class);
            Call<Common> call = null;
            if (registerType.equals("1"))
                call = apiInterface.registerFacebook(email, registerType, fbId, token);
            else if (registerType.equals("2"))
                call = apiInterface.registerGoogle(email, registerType, gPlusId, name, token);
            call.enqueue(new Callback<Common>() {
                @Override
                public void onResponse(Call<Common> call, Response<Common> response) {
                    pd.dismiss();
                    Common c = response.body();
                    Log.e("In LoginActivity", "Response : " + new Gson().toJson(c));
                    if (c.getCode() == 100) {
//                        Config.saveSharedPreferences(LoginActivity.this, "userId", c.getUserId());
                        // As only user can register, user type will always be 1
                        Config.saveSharedPreferences(LoginActivity.this, "userId", c.getUserIdFromServer());
                        Config.saveSharedPreferences(LoginActivity.this, "email", email);
                        Config.saveSharedPreferences(LoginActivity.this, "userType", "1");
                        final DialogPlus dialog = DialogPlus.newDialog(LoginActivity.this)
                                .setContentHolder(new ViewHolder(R.layout.dialog_ok_layout))
                                .setContentHeight(LinearLayout.LayoutParams.WRAP_CONTENT)
                                .setGravity(Gravity.CENTER)
                                .create();
                        dialog.show();

                        View errorView = dialog.getHolderView();
                        TextView titleTv = (TextView) errorView.findViewById(R.id.titleTv);
                        TextView messageTv = (TextView) errorView.findViewById(R.id.messageTv);
                        Button okBtn = (Button) errorView.findViewById(R.id.okBtn);
                        titleTv.setVisibility(View.GONE);
                        messageTv.setText(c.getMessage());
//                        messageTv.setText("Please verify your account as it is not yet verified. After that you need to login to proceed further");
                        okBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                finish();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });

                    } else {
                        Config.showDialog(LoginActivity.this, c.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<Common> call, Throwable t) {
                    pd.dismiss();
                    Log.e("In LoginActivity", "OnFailure Excp : " + t.getMessage());
                    Config.showDialog(LoginActivity.this, Config.FailureMsg);
                }
            });
        } else {
            Config.showAlertForInternet(LoginActivity.this);
        }
    }

    public void sendActualDataToDb(final String email, final String userIdFromServer) {
        final RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
        Call<Common> call = apiInterface.getProfile(email, userIdFromServer);
        call.enqueue(new Callback<Common>() {
            @Override
            public void onResponse(Call<Common> call, Response<Common> response) {
                Common c = response.body();
                Log.e("In SendActualData", "Response : " + new Gson().toJson(c));
                if (c != null && c.getCode() == 100) {
                    UserInfo ui = c.getUserInfo();
                    String fullName = "";
                    if (ui.getFullName().contains(" ")) {
                        fullName = ui.getFullName().split(" ")[0];
                    } else {
                        fullName = ui.getFullName();
                    }
                    Call<Common> call1 = apiInterface.registerUsingDb(fullName, email, "", ui.getMobileNo(), token,
                            "0");
                    call1.enqueue(new Callback<Common>() {
                        @Override
                        public void onResponse(Call<Common> call, Response<Common> response) {
                            Common c1 = response.body();
                            if (c1.getSuccess() == 1) {
                                pd.dismiss();
                                Config.saveSharedPreferences(LoginActivity.this, "userId", c1.getUserId());
                                Config.saveSharedPreferences(LoginActivity.this, "userType", "1");
                                finish();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            } else {
                                pd.dismiss();
                                Log.e("In SendActualData", "In ELse Success : " + c1.getSuccess());
                                Config.showDialog(LoginActivity.this, c1.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<Common> call, Throwable t) {
                            pd.dismiss();
                            Log.e("In SendActualData", "OnFailure Excp : " + t.getMessage());
                            Config.showDialog(LoginActivity.this, Config.FailureMsg);
                        }
                    });
                }

            }

            @Override
            public void onFailure(Call<Common> call, Throwable t) {
                pd.dismiss();
                Log.e("In SendActualData", "Get Profile OnFailure Excp : " + t.getMessage());
                Config.showDialog(LoginActivity.this, Config.FailureMsg);
            }
        });
    }

}
