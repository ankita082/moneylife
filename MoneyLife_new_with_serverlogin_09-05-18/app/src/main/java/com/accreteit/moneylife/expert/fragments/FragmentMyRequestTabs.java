package com.accreteit.moneylife.expert.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.accreteit.moneylife.MainActivity;
import com.accreteit.moneylife.R;

import java.util.List;

public class FragmentMyRequestTabs extends Fragment {

    TextView toolbarTitle;
    ImageView backIv;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter pagerAdapter;
    SearchView searchView;

    public FragmentMyRequestTabs() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_request_tabs, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        setHasOptionsMenu(true);

        initToolbar();

        tabLayout = rootView.findViewById(R.id.tabLayout);
        viewPager = rootView.findViewById(R.id.viewPager);
        pagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
//        setUpTabIcons();

        return rootView;
    }

    public void initToolbar() {
        toolbarTitle = ((MainActivity) getActivity()).toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("My Requests");
        backIv = ((MainActivity) getActivity()).toolbar.findViewById(R.id.backIv);

        backIv.setImageResource(R.drawable.ic_toolbar_logo);
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        getActivity().getMenuInflater().inflate(R.menu.menu_home, menu);

        final MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        int searchImgId = android.support.v7.appcompat.R.id.search_button; // I used the explicit layout ID of searchview's ImageView
        ImageView v = (ImageView) searchView.findViewById(searchImgId);
        v.setImageResource(R.drawable.ic_search);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
//                Log.e("In FrgmtMyReqTabs", "OnQuerySubmit : " + query);
                int tabPos = tabLayout.getSelectedTabPosition();
//                Log.e("In FrgmtMyReqTabs", "Tab Sel : " + tabPos);
                searchMenuItem.collapseActionView();
                changeListData(tabPos, query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
//                Log.e("In FrgmtMyReqTabs", "OnQueryChange : " + s);
                int tabPos = tabLayout.getSelectedTabPosition();
//                Log.e("In FrgmtMyReqTabs", "Tab Sel : " + tabPos);
                changeListData(tabPos, s);
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                searchMenuItem.collapseActionView();
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_search) {

        } else {
            getActivity().onBackPressed();
        }
        return true;
    }

    public void changeListData(int tabPos, String query) {
        List<Fragment> fragmentList = getChildFragmentManager().getFragments();
        for (Fragment f : fragmentList) {
            if (tabPos == 0 && f instanceof FragmentOpenQuestions) {
                ((FragmentOpenQuestions) f).adapter.getFilter().filter(query);
            }
            if (tabPos == 1 && f instanceof FragmentAnsweredQuestions) {
                ((FragmentAnsweredQuestions) f).adapter.getFilter().filter(query);
            }
        }
    }


    private void setUpTabIcons() {
//        tabLayout.getTabAt(0).setIcon(R.drawable.ic_dashboard);
//        tabLayout.getTabAt(1).setIcon(R.drawable.ic_bitcoin_wallet);
//        tabLayout.getTabAt(2).setIcon(R.drawable.ic_inr_wallet);
    }


    public class ViewPagerAdapter extends FragmentPagerAdapter {
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            Bundle bundle = null;
            switch (position) {
                case 0:
                    fragment = new FragmentOpenQuestions();
//                    bundle = new Bundle();
//                    bundle.putString("filterJson", filterJson);
//                    fragment.setArguments(bundle);
                    return fragment;

                case 1:
                    fragment = new FragmentAnsweredQuestions();
//                    bundle = new Bundle();
//                    bundle.putString("filterJson", filterJson);
//                    fragment.setArguments(bundle);
                    return fragment;

                default:
                    return null;
            }

        }


        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Open";
                case 1:
                    return "Answered";
            }
            return null;
        }


        @Override
        public int getCount() {
            return 2;
        }
    }


}
