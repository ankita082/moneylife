package com.accreteit.moneylife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.accreteit.moneylife.adapters.QuestionsAdapter;
import com.accreteit.moneylife.responses.GetQuestions;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyQuestions extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolbarTitle, noDataTv;
    ImageView backIv;
    RecyclerView recyclerView;
    QuestionsAdapter adapter;
    String questionJson = "{ \"code\": 100, \"query\": [ { \"question_id\": 1, \"question\": \"Ask Question Here\", \"question_image\": \"https://picsum.photos/458/354/?image=882\", \"expert_name\": \"Misthy Shah\", \"comments_number\":\"3\" }, { \"question_id\": 2, \"question\": \"Ask Question Here\", \"question_image\": \"https://picsum.photos/458/354\", \"expert_name\": \"Gunjan Mehta\", \"comments_number\":\"9\" }, { \"question_id\": 3, \"question\": \"Ask Question Here\", \"question_image\": \"https://picsum.photos/1263/285/?image=933\", \"expert_name\": \"Dev Dixit\", \"comments_number\":\"15\" }, { \"question_id\": 4, \"question\": \"Ask Question Here\", \"question_image\": \"https://picsum.photos/458/354?gravity=east\", \"expert_name\": \"Somil Vyas\", \"comments_number\":\"22\" } ] }";
    ProgressDialog pd;
    GetQuestions q;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_questions);

        initToolbar();

        noDataTv = findViewById(R.id.noDataTv);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(MyQuestions.this));

        if(Config.isConnectedToInternet(MyQuestions.this)){
            pd = new ProgressDialog(MyQuestions.this);
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.show();

            RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
            Call<GetQuestions> call = apiInterface.getUsersQuestions(Config.getSharedPreferences(MyQuestions.this, "userId"));
            call.enqueue(new Callback<GetQuestions>() {
                @Override
                public void onResponse(Call<GetQuestions> call, Response<GetQuestions> response) {
                    pd.dismiss();
                    q = response.body();
                    if (q.getCode() == 100) {
                        noDataTv.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        adapter = new QuestionsAdapter(MyQuestions.this, q.getQuestionList());
                        recyclerView.setAdapter(adapter);
                    } else {
                        noDataTv.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<GetQuestions> call, Throwable t) {
                    pd.dismiss();
                    Log.e("In MyQuestions", "OnFailure Excp : " + t.getMessage());
                    Config.showDialog(MyQuestions.this, Config.FailureMsg);
                }
            });
        } else {
            Config.showAlertForInternet(MyQuestions.this);
        }


    }

    public void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("My Questions");
        backIv = toolbar.findViewById(R.id.backIv);

        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_search) {
        } else {
            onBackPressed();
        }
        return true;
    }


}
