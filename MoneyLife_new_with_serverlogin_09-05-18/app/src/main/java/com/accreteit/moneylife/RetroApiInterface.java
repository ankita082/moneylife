package com.accreteit.moneylife;


import com.accreteit.moneylife.models.Expert;
import com.accreteit.moneylife.models.Question;
import com.accreteit.moneylife.responses.AddComment;
import com.accreteit.moneylife.responses.Common;
import com.accreteit.moneylife.responses.GetAllCategories;
import com.accreteit.moneylife.responses.GetAllNotifications;
import com.accreteit.moneylife.responses.GetExperts;
import com.accreteit.moneylife.responses.GetQuestions;
import com.accreteit.moneylife.responses.GetQuestionsForExperts;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RetroApiInterface {
    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json",
            "Content-Type: application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    @POST("login.html")
    public Call<Common> loginUsingServer(@Field("email") String email, @Field("password") String password,
                                         @Field("device_token") String deviceToken);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @FormUrlEncoded
    @POST("signup.html")
    public Call<Common> registerUsingServer(@Field("name") String name, @Field("email") String email,
                                            @Field("password") String password, @Field("device_type") String deviceType,
                                            @Field("account_type") String registerType,
                                            @Field("device_token") String deviceToken);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @FormUrlEncoded
    @POST("signup.html")
    public Call<Common> registerFacebook(@Field("email") String email, @Field("account_type") String registerType,
                                         @Field("facebook_uid") String facebookId, @Field("device_token") String deviceToken);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @FormUrlEncoded
    @POST("signup.html")
    public Call<Common> registerGoogle(@Field("email") String email, @Field("account_type") String registerType,
                                       @Field("google_uid") String gPlusId, @Field("name") String gPlusName,
                                       @Field("device_token") String deviceToken);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @FormUrlEncoded
    @POST("forgotpwd.html")
    public Call<Common> forgotPassword(@Field("email") String email);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @Multipart
    @POST("profileupdate.html")
    public Call<Common> updateProfileWithMultipart(@Query("email") String email, @Query("userid") String userid,
                                                   @Query("user_name") String username,
                                                   @Query("full_name") String full_name,
                                                   @Query("mobile_no") String mobile_no,
                                                   @Part MultipartBody.Part image);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @FormUrlEncoded
    @POST("profileupdate.html")
    public Call<Common> updateProfileWithoutMultipart(@Field("email") String email, @Field("userid") String userid,
                                                      @Field("user_name") String username,
                                                      @Field("full_name") String full_name,
                                                      @Field("mobile_no") String mobile_no);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @FormUrlEncoded
    @POST("profileinfo.html")
    public Call<Common> getProfile(@Field("email") String email, @Field("userid") String userid);

    @Headers({
            "Authorization: d46676584983472c8d2e70115cca62ec",
            "Accept: application/json"
    })
    @FormUrlEncoded
    @POST("changepassword.html")
    public Call<Common> changePassword(@Field("email") String email, @Field("userid") String userid, @Field("oldpassword") String username,
                                       @Field("newpassword") String newPassword);

    @POST("login.php")
    public Call<Common> loginUsingDb(@Query("email") String email, @Query("password") String password,
                                     @Query("device_id") String deviceId, @Query("device_type") String deviceType);

    @POST("signup.php")
    public Call<Common> registerUsingDb(@Query("firstName") String firstName, @Query("email") String email,
                                        @Query("password") String password, @Query("mobile_no") String mobileNumber,
                                        @Query("device_id") String deviceId, @Query("device_type") String deviceType);

    @Multipart
    @POST("ask_query.php")
    public Call<Common> askQuery(@Query("query_topic") String queryTopic, @Query("query") String query,
                                 @Query("user_id") String user_id, @Query("expert_id") String expert_id, @Part MultipartBody.Part video,
                                 @Part List<MultipartBody.Part> attachments, @Query("total_attachments") String total_attachments);

    @POST("ask_query.php")
    public Call<Common> askQueryWithoutMultipart(@Query("query_topic") String queryTopic, @Query("query") String query,
                                                 @Query("user_id") String user_id, @Query("expert_id") String expert_id);

    @Multipart
    @POST("answer_the_question.php")
    public Call<Common> answerQuery(@Query("query_id") String queryId, @Query("expert_id") String expert_id,
                                    @Query("answer_topic") String replyTitle, @Query("answer") String reply,
                                    @Part MultipartBody.Part video, @Part List<MultipartBody.Part> attachments,
                                    @Query("total_attachments") String total_attachments);

    @POST("answer_the_question.php")
    public Call<Common> answerQueryWithoutMultipart(@Query("query_id") String queryId, @Query("expert_id") String expert_id,
                                                    @Query("answer_topic") String replyTitle, @Query("answer") String reply);

    @POST("list_category.php")
    public Call<GetAllCategories> getCategories();

    @POST("list_notification.php")
    public Call<GetAllNotifications> getAllNotifications(@Query("user_id") String userId, @Query("user_type") String userType);

    @POST("list_expert.php")
    public Call<GetExperts> getAllExperts();

    @FormUrlEncoded
    @POST("list_question.php")
    public Call<GetQuestions> getAllQuestions(@Field("category_id") String categoryId);

    @FormUrlEncoded
    @POST("list_user_question.php")
    public Call<GetQuestions> getUsersQuestions(@Field("user_id") String userId);

//    @FormUrlEncoded
//    @POST("change_password.php")
//    public Call<Common> changePassword(@Field("user_id") String userId, @Field("old_password") String oldPassword, @Field("new_password") String newPassword);

    @FormUrlEncoded
    @POST("single_question.php")
    public Call<Question> getQuestionDetails(@Field("query_id") String queryId);

    @POST("expert_details.php")
    public Call<Expert> getExpertDetails(@Query("expert_id") String expertId);

    @POST("expert_request_open.php")
    public Call<GetQuestionsForExperts> getUnansweredQuestions(@Query("expert_id") String expertId);

    @POST("expert_request_answered.php")
    public Call<GetQuestionsForExperts> getAnsweredQuestions(@Query("expert_id") String expertId);

    @POST("add_comment.php")
    public Call<AddComment> addComment(@Query("user_id") String userId, @Query("user_type") String userType,
                                       @Query("query_id") String queryId, @Query("comment") String comment);
}
