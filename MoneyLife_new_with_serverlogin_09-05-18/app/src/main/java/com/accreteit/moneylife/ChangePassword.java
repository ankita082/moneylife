package com.accreteit.moneylife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.accreteit.moneylife.responses.Common;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.accreteit.moneylife.RetroApiClient.okHttpClient;

public class ChangePassword extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolbarTitle;
    ImageView backIv;
    LinearLayout editLayout;
    TextInputLayout oldPasswordInput, newPasswordInput, confirmPasswordInput;
    EditText confirmPasswordEt, newPasswordEt, oldPasswordEt;
    TextView updateBtn;
    String oldPassword = "", newPassword = "", confirmPassword = "";
    Boolean isValid;
    ProgressDialog pd;
    int count = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        initToolbar();

        editLayout = findViewById(R.id.editLayout);
        oldPasswordInput = findViewById(R.id.oldPasswordInput);
        newPasswordInput = findViewById(R.id.newPasswordInput);
        confirmPasswordInput = findViewById(R.id.confirmPasswordInput);
        oldPasswordEt = findViewById(R.id.oldPasswordEt);
        newPasswordEt = findViewById(R.id.newPasswordEt);
        confirmPasswordEt = findViewById(R.id.confirmPasswordEt);
        updateBtn = findViewById(R.id.updateBtn);

        changeEnabledStatus(false);

        editLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ++count;
                if (count % 2 == 0) {
                    changeEnabledStatus(true);
                } else {
                    changeEnabledStatus(false);
                }
            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (count % 2 == 0) {
                    isValid = true;
                    oldPassword = oldPasswordEt.getText().toString();
                    newPassword = newPasswordEt.getText().toString();
                    confirmPassword = confirmPasswordEt.getText().toString();
                    oldPasswordInput.setErrorEnabled(false);
                    newPasswordInput.setErrorEnabled(false);
                    confirmPasswordInput.setErrorEnabled(false);

                    if (oldPassword.equalsIgnoreCase("")) {
                        isValid = false;
                        oldPasswordInput.setErrorEnabled(true);
                        oldPasswordInput.setError("Must enter old password");
                    }
                    if (newPassword.equalsIgnoreCase("")) {
                        isValid = false;
                        newPasswordInput.setErrorEnabled(true);
                        newPasswordInput.setError("Must enter new password");
                    }
                    if (confirmPassword.equalsIgnoreCase("")) {
                        isValid = false;
                        confirmPasswordInput.setErrorEnabled(true);
                        confirmPasswordInput.setError("Must enter confirm password");
                    } else {
                        if (!confirmPassword.equals(newPassword)) {
                            isValid = false;
                            confirmPasswordInput.setErrorEnabled(true);
                            confirmPasswordInput.setError("Confirm password and password must be same");
                        }
                    }

                    if (isValid) {
                        pd = new ProgressDialog(ChangePassword.this);
                        pd.setTitle("Please wait");
                        pd.setMessage("Loading..");
                        pd.setCancelable(false);
                        pd.show();
                        if (Config.isConnectedToInternet(ChangePassword.this)) {
                            Retrofit retro = new Retrofit.Builder()
                                    .baseUrl(Config.SignUpnLoginUrl)
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .client(okHttpClient)
                                    .build();
                            String userId = Config.getSharedPreferences(ChangePassword.this, "userId");
                            String email = Config.getSharedPreferences(ChangePassword.this, "email");
                            Log.e("In ChangePassword", "Params : UserId -> " + userId + ", Email -> " + email
                                    + ", OldPwd -> " + oldPassword + " n NewPwd -> " + newPassword);
                            RetroApiInterface apiInterface = retro.create(RetroApiInterface.class);
                            Call<Common> call = apiInterface.changePassword(email, userId, oldPassword, newPassword);
                            call.enqueue(new Callback<Common>() {
                                @Override
                                public void onResponse(Call<Common> call, Response<Common> response) {
                                    pd.dismiss();
                                    Common c = response.body();
                                    if (c != null && c.getCode() == 100) {
                                        Config.showDialog(ChangePassword.this, c.getMessage());
                                    } else {
                                        Config.showDialog(ChangePassword.this, c.getMessage());
                                    }
                                }

                                @Override
                                public void onFailure(Call<Common> call, Throwable t) {
                                    pd.dismiss();
                                    Log.e("In ChangePassword", "In OnFailure Msg : " + t.getMessage());
                                    Config.showDialog(ChangePassword.this, Config.FailureMsg);
                                }
                            });

                        } else {
                            Config.showAlertForInternet(ChangePassword.this);
                        }
                    }
                } else {
                    Config.showDialog(ChangePassword.this, "Please click on edit to change password");
                }
            }
        });

    }

    public void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Change Password");
        backIv = toolbar.findViewById(R.id.backIv);

        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void changeEnabledStatus(boolean status) {
        oldPasswordEt.setEnabled(status);
        newPasswordEt.setEnabled(status);
        confirmPasswordEt.setEnabled(status);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

}
