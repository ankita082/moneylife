package com.accreteit.moneylife.responses;

import com.accreteit.moneylife.models.Category;
import com.accreteit.moneylife.models.Notification;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 22-Mar-18.
 */

public class GetAllNotifications {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("notification")
    @Expose
    private List<Notification> notificationList = null;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<Notification> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(List<Notification> notificationList) {
        this.notificationList = notificationList;
    }

}
