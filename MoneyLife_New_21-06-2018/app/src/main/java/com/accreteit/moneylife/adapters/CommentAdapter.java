package com.accreteit.moneylife.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.accreteit.moneylife.R;
import com.accreteit.moneylife.models.Comment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;


public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {
    List<Comment> data;
    private LayoutInflater inflater;
    private Context context;

    public CommentAdapter(Context context, List<Comment> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_comment_list, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Comment c = data.get(position);
        Glide.with(context)
                .load(c.getImage())
                .apply(new RequestOptions().circleCrop()
                        .placeholder(R.drawable.image_loader)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true))
                .into(holder.imageIv);
        holder.nameTv.setText(c.getName());
        holder.descriptionTv.setText(c.getComment());
        holder.timeTv.setText(c.getTime());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageIv;
        TextView timeTv, nameTv, descriptionTv;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageIv = (ImageView) itemView.findViewById(R.id.imageIv);
            timeTv = (TextView) itemView.findViewById(R.id.timeTv);
            nameTv = (TextView) itemView.findViewById(R.id.nameTv);
            descriptionTv = (TextView) itemView.findViewById(R.id.descriptionTv);
        }
    }
}
