package com.accreteit.moneylife.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.accreteit.moneylife.Config;
import com.accreteit.moneylife.MainActivity;
import com.accreteit.moneylife.R;
import com.accreteit.moneylife.RetroApiClient;
import com.accreteit.moneylife.RetroApiInterface;
import com.accreteit.moneylife.models.Expert;
import com.accreteit.moneylife.player.FullscreenVideoLayout;
import com.accreteit.moneylife.responses.Common;
import com.accreteit.moneylife.responses.GetExperts;
import com.google.gson.Gson;
import com.iceteck.silicompressorr.SiliCompressor;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

// import com.vincent.videocompressor.VideoCompress;


public class FragmentQuery extends Fragment {

    TextView toolbarTitle;
    ImageView backIv;
    static final int PERMISSION_REQ_CODE = 100;
    static final int REQUEST_VIDEO_CAPTURE = 1;
    static final int PICKFILE_REQUEST_CODE = 2;
    static final int PICKFILE_REQUEST_VIDEO = 3;
    LinearLayout recordVideoLayout;
    LinearLayout uploadFilesLayout;
    LinearLayout uploadAttachmentsLayout;
    EditText topicEt, queryEt;
    TextView submitTv, spinnerErrorTv;
    TextInputLayout topicLayout, queryLayout;
    Spinner expertSpinner;
    ProgressDialog pd;
    MultipartBody.Part video;
    ArrayList<MultipartBody.Part> attachmentList = new ArrayList<>();
    LayoutInflater attachmentInflater;
    RelativeLayout videoLayout;
    FullscreenVideoLayout queryVideoView;
    static int i = 0;
    String[] mimeTypes =
            {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                    "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                    "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                    "text/plain",
                    "application/pdf",
                    "application/zip"};
    String videoUrl = "", title = "", query = "", fromBtnClick = "", destinationPath = ""; // For compressed video;
    public int expertId = 0;
    HashMap<Integer, String> urlList = new HashMap<Integer, String>();
    List<Expert> expertList;
    LinearLayout fileLayout;
    File destinationFolder;
    ProgressBar pb;
    TextView progressTv;
    long startTime, endTime;

    public FragmentQuery() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_query, container, false);

        recordVideoLayout = rootView.findViewById(R.id.recordVideoLayout);
        uploadFilesLayout = rootView.findViewById(R.id.uploadFilesLayout);
        topicEt = rootView.findViewById(R.id.topicEt);
        queryEt = rootView.findViewById(R.id.queryEt);
        topicLayout = rootView.findViewById(R.id.topicLayout);
        queryLayout = rootView.findViewById(R.id.queryLayout);
        uploadAttachmentsLayout = rootView.findViewById(R.id.uploadAttachmentsLayout);
        submitTv = rootView.findViewById(R.id.submitTv);
        expertSpinner = rootView.findViewById(R.id.expertSpinner);
        spinnerErrorTv = rootView.findViewById(R.id.spinnerErrorTv);
        fileLayout = rootView.findViewById(R.id.fileLayout);
        videoLayout = rootView.findViewById(R.id.videoLayout);
        queryVideoView = rootView.findViewById(R.id.queryVideoView);
        pb = rootView.findViewById(R.id.pb);
        progressTv = rootView.findViewById(R.id.progressTv);

        attachmentInflater = LayoutInflater.from(getContext());

        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        initToolbar();
        setHasOptionsMenu(true);

        videoLayout.setVisibility(View.GONE);
        spinnerErrorTv.setVisibility(View.GONE);
        pb.setVisibility(View.GONE);
        progressTv.setVisibility(View.GONE);

        if (Config.isConnectedToInternet(getActivity())) {
            pd = new ProgressDialog(getActivity());
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();

            RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
            Call<GetExperts> call = apiInterface.getAllExperts();
            call.enqueue(new Callback<GetExperts>() {
                @Override
                public void onResponse(Call<GetExperts> call, Response<GetExperts> response) {
                    pd.dismiss();
                    GetExperts es = response.body();
                    Log.e("In FrgmntQuery", "In GetAllExperts Response : " + new Gson().toJson(es));
                    if (es.getExpertList().size() > 0) {
                        expertList = es.getExpertList();
                        List<String> expertsNameList = new ArrayList<>();
                        expertsNameList.add("Select Expert");
                        for (Expert e : expertList) {
                            expertsNameList.add(e.getName());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_list_item_1, expertsNameList);
                        expertSpinner.setAdapter(adapter);
                    }
                }

                @Override
                public void onFailure(Call<GetExperts> call, Throwable t) {
                    pd.dismiss();
                    Log.e("In FrgmtQuery", "OnFailure Excp : " + t.getMessage());
                    Config.showDialog(getActivity(), Config.FailureMsg);
                }
            });
        } else {
            Config.showAlertForInternet(getActivity());
        }

        recordVideoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromBtnClick = "Record";
                if (checkPermissions()) {
                    if (videoUrl.equalsIgnoreCase("")) {
                        dispatchTakeVideoIntent();
                    } else {
                        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                                .setTitle("Confirmation")
                                .setMessage("By clicking continue, uploaded video will be replaced with the new video.")
                                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        queryVideoView.reset();
                                        dispatchTakeVideoIntent();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .setCancelable(true)
                                .create();
                        dialog.show();
                    }
                }
            }
        });

        uploadFilesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromBtnClick = "Upload";
                if (checkPermissions()) {
                    if (videoUrl == null || videoUrl.equalsIgnoreCase("")) {
                        dispatchSelectVideoIntent();
                    } else {
                        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                                .setTitle("Confirmation")
                                .setMessage("By clicking continue, uploaded video will be replaced with the new video.")
                                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dispatchSelectVideoIntent();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .setCancelable(true)
                                .create();
                        dialog.show();
                    }
                }
            }
        });

        uploadAttachmentsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromBtnClick = "Attach";
                if (checkPermissions()) {
                    dispatchTakeFilesIntent();
                }
            }
        });

        submitTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isValid = true;
                title = topicEt.getText().toString();
                query = queryEt.getText().toString();
                String selExpertName = ((String) expertSpinner.getSelectedItem());
                for (Expert e : expertList) {
                    if (e.getName().equals(selExpertName)) {
                        expertId = e.getId();
                    }
                }
                topicLayout.setErrorEnabled(false);
                queryLayout.setErrorEnabled(false);
                spinnerErrorTv.setVisibility(View.GONE);
                Log.e("In FrgmntQuery", "Selected Expert Id : " + expertId);

                if (title.equalsIgnoreCase("")) {
                    isValid = false;
                    topicLayout.setErrorEnabled(true);
                    topicLayout.setError("Must enter short description for question");
                }
                if (query.equalsIgnoreCase("")) {
                    isValid = false;
                    queryLayout.setErrorEnabled(true);
                    queryLayout.setError("Must enter long description for question");
                }
                if (expertId == 0) {
                    isValid = false;
                    spinnerErrorTv.setVisibility(View.VISIBLE);
                }

                if (!videoUrl.equalsIgnoreCase("")) {
                    video = prepareFilePart("query_video", Uri.parse(videoUrl), "1");
                }
                if (urlList.size() > 0) {
                    int x = 1;
                    for (Integer key : urlList.keySet()) {
                        String url = urlList.get(key);
                        attachmentList.add(prepareFilePart("attachment_" + x, Uri.parse(url), "0"));
                        x++;
                    }
                }

                if (isValid) {
                    if (!videoUrl.equalsIgnoreCase(""))
                        // compressVideo(videoUrl, destinationFolder.getPath() + "/" + System.currentTimeMillis() + ".mp4");
                        new VideoCompressAsyncTask(getActivity()).execute(videoUrl, destinationFolder.getPath());

                }
            }
        });

        return rootView;
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri, String isVideo) {
        File file = new File(fileUri.getPath());
        RequestBody requestFile = null;
        if (isVideo.equals("0")) {
            if (file != null)
                requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        } else if (isVideo.equals("1")) {
            if (file != null)
                requestFile = RequestBody.create(MediaType.parse("video/*"), file);
        }

        if (requestFile != null) {
            // MultipartBody.Part is used to send also the actual file name
            return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
        } else {
            Log.e("Exception", "Exception : File is null");
        }
        return null;

    }


    public void initToolbar() {
        toolbarTitle = ((MainActivity) getActivity()).toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Ask Your Query");
        backIv = ((MainActivity) getActivity()).toolbar.findViewById(R.id.backIv);

        backIv.setImageResource(R.drawable.ic_toolbar_logo);
//        backIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getActivity().onBackPressed();
//            }
//        });
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    private void dispatchTakeFilesIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
//        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
    }

    private void dispatchSelectVideoIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("video/*");
        startActivityForResult(intent, PICKFILE_REQUEST_VIDEO);
    }

    /* public String getRealPathFromURI(Context context, Uri contentUri, String isVideo) {
        Cursor cursor = null;
        try {
            if (isVideo.equals("0")) {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                Log.e("In FrgmntQuery", "In GetRealPath ImagePath : " + cursor.getString(column_index));
                return cursor.getString(column_index);
            } else if (isVideo.equals("1")) {
                String[] projection = {MediaStore.Video.Media.DATA};
                cursor = context.getContentResolver().query(contentUri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                cursor.moveToFirst();
                Log.e("In FrgmntQuery", "In GetRealPath VideoPath : " + cursor.getString(column_index));
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            return null;
        }
    } */

    public static String getFilePath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public void resetAllViews() {
        queryVideoView.reset();
        videoLayout.setVisibility(View.GONE);
        urlList.clear();
        attachmentList.clear();
        fileLayout.removeAllViews();
        video = null;
        videoUrl = "";
        queryEt.setText("");
        topicEt.setText("");
        topicEt.requestFocus();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        try {
            if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
                Uri videoUri = intent.getData();
//                videoUrl = getRealPathFromURI(getActivity(), videoUri, "1");
                videoUrl = getFilePath(getActivity(), videoUri);

                destinationFolder = new File(Environment.getExternalStorageDirectory(),
                        "/MoneyLife/User/Videos");
                destinationFolder.mkdirs();
                Log.e("In OnActResult", "In IF FolderPath : " + destinationFolder.getPath());
                Log.e("In OnActResult", "In IF VideoPath : " + videoUrl);

//                compressVideo();
//                Log.e("In OnActResult", "In IF OutputPath : " + filePath);
                videoLayout.setVisibility(View.VISIBLE);
//                try {
                queryVideoView.reset();
                queryVideoView.setVideoURI(videoUri);
//                } catch (IOException e) {
//                    Log.e("Video URL", "Video Exception : " + e.getMessage());
//                }
                queryVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        queryVideoView.seekTo(500);
                    }
                });
            } else if (requestCode == PICKFILE_REQUEST_CODE && resultCode == RESULT_OK) {
                Uri docUri = intent.getData();
//                String url = getRealPathFromURI(getActivity(), docUri, "0");
                String url = getFilePath(getActivity(), docUri);
                Log.e("In OnActResult", "In Else IF ImagePath : " + url);
                if (urlList.containsValue(url)) {
                    Toast.makeText(getActivity(), "File already Selected", Toast.LENGTH_SHORT).show();
                } else {
                    View view = attachmentInflater.inflate(R.layout.row_uploaded_attachments, null);
                    TextView fileNameTv = view.findViewById(R.id.fileNameTv);
                    ImageView closeIv = view.findViewById(R.id.closeIv);
                    urlList.put(i, url);
                    closeIv.setTag(i);
                    ++i;
                    fileNameTv.setText(docUri.getLastPathSegment());
                    closeIv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int position = (int) v.getTag();
                            urlList.remove(position);
                            Log.e("In FrgmntQuery", "In AddAttachment UrlList : " + urlList.size());
                            fileLayout.removeView((View) v.getParent());
                        }
                    });
                    fileLayout.addView(view);
                }
            } else if (requestCode == PICKFILE_REQUEST_VIDEO && resultCode == RESULT_OK) {
//                videoUrl = getRealPathFromURI(getActivity(), intent.getData(), "1");
                videoUrl = getFilePath(getActivity(), intent.getData());

                destinationFolder = new File(Environment.getExternalStorageDirectory(),
                        "/MoneyLife/User/Videos");
                destinationFolder.mkdirs();
                Log.e("In OnActResult", "In Else IF1 FolderPath : " + destinationFolder.getPath());
                Log.e("In OnActResult", "In Else IF1 VideoPath : " + videoUrl);

                videoLayout.setVisibility(View.VISIBLE);
//                try {
                queryVideoView.reset();
                queryVideoView.setVideoURI(intent.getData());
//                } catch (IOException e) {
//                    Log.e("Video URL", "Video Exception : " + e.getMessage());
//                }
                queryVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        queryVideoView.seekTo(500);
                    }
                });

//                File compressedFile = new File(Environment.getExternalStorageDirectory(),
//                        "/MoneyLife/User/Videos/" + System.currentTimeMillis() + ".mp4");
//                compressedFile.createNewFile();
//                String filePath = SiliCompressor.with(getActivity())
//                        .compressVideo(videoUrl, folder.getPath());
//                compressVideo(videoUrl, folder.getPath()+ "/" + System.currentTimeMillis() + ".mp4");
//                Log.e("In OnActResult", "In Else IF1 OutputPath : " + filePath);
//                new VideoCompressAsyncTask(getActivity()).execute(videoUrl, folder.getPath());
            }
        } catch (Exception e) {
            Log.e("In OnActResult", "Exception : " + e.getMessage());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQ_CODE) {
            if (grantResults.length > 0) {
                Log.e("In FrgmntQuery", "In OnRequest In IF");
                if (fromBtnClick.equals("Record")) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                            grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                            grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                        Log.e("In FrgmntQuery", "In Inner IF : " + fromBtnClick);
                        if (videoUrl == null || videoUrl.equalsIgnoreCase("")) {
                            dispatchTakeVideoIntent();
                        } else {
                            AlertDialog dialog = new AlertDialog.Builder(getActivity())
                                    .setTitle("Confirmation")
                                    .setMessage("By clicking continue, uploaded video will be replaced with the new video.")
                                    .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            queryVideoView.reset();
                                            dispatchTakeVideoIntent();
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setCancelable(true)
                                    .create();
                            dialog.show();
                        }
                    }
//                    else {
//                        Log.e("In FrgmntQuery", "In Inner ELSE : " + fromBtnClick);
//                        runtimePermissions();
//                    }
                } else if (fromBtnClick.equals("Upload") || fromBtnClick.equals("Attach")) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                            grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        Log.e("In FrgmntQuery", "In Inner IF : " + fromBtnClick);
                        if (fromBtnClick.equals("Upload")) {
                            if (videoUrl == null || videoUrl.equalsIgnoreCase("")) {
                                dispatchSelectVideoIntent();
                            } else {
                                AlertDialog dialog = new AlertDialog.Builder(getActivity())
                                        .setTitle("Confirmation")
                                        .setMessage("By clicking continue, uploaded video will be replaced with the new video.")
                                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                queryVideoView.reset();
                                                dispatchSelectVideoIntent();
                                            }
                                        })
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .setCancelable(true)
                                        .create();
                                dialog.show();
                            }
                        } else {
                            dispatchTakeFilesIntent();
                        }
                    }
//                    else {
//                        Log.e("In FrgmntQuery", "In Inner ELSE : " + fromBtnClick);
//                        runtimePermissions();
//                    }
                }
            }
        }
    }

    public boolean checkPermissions() {
        if (fromBtnClick.equals("Record")) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                runtimePermissions();
                return false;
            }
        } else if (fromBtnClick.equals("Upload") || fromBtnClick.equals("Attach")) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                runtimePermissions();
                return false;
            }
        } else {
            runtimePermissions();
            return false;
        }
    }

    public void runtimePermissions() {
        if (fromBtnClick.equals("Record"))
            requestPermissions(new String[]{Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQ_CODE);
        else
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSION_REQ_CODE);
    }

    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {
        Context mContext;

        public VideoCompressAsyncTask(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();
//            imageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_photo_camera_white_48px));
//            compressionMsg.setVisibility(View.VISIBLE);
//            picDescription.setVisibility(View.GONE);
        }

        @Override
        protected String doInBackground(String... paths) {
            String filePath = null;
            try {
                filePath = SiliCompressor.with(mContext).compressVideo(paths[0], paths[1]);

            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return filePath;

        }


        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
            pd.dismiss();
//            File imageFile = new File(compressedFilePath);
//            float length = imageFile.length() / 1024f; // Size in KB
//            String value;
//            if(length >= 1024)
//                value = length/1024f+" MB";
//            else
//                value = length+" KB";
//            String text = String.format(Locale.US, "%s\nName: %s\nSize: %s", getString(R.string.video_compression_complete), imageFile.getName(), value);
//            compressionMsg.setVisibility(View.GONE);
//            picDescription.setVisibility(View.VISIBLE);
//            picDescription.setText(text);
            Log.e("Silicompressor", "Path: " + compressedFilePath);
        }
    }

    /*public void compressVideo(String inputPath, String outputPath) {
//        pb = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleSmall);
//        pb.setVisibility(View.VISIBLE);
        VideoCompress.compressVideoLow(inputPath, outputPath, new VideoCompress.CompressListener() {
            @Override
            public void onStart() {
                Log.e("In CompressVideo", "In OnStart");
                //Start Compress
            }

            @Override
            public void onSuccess() {
                Log.e("In CompressVideo", "In OnSuccess");
                //Finish successfully
            }

            @Override
            public void onFail() {
                Log.e("In CompressVideo", "In OnFail");
                //Failed
            }

            @Override
            public void onProgress(float percent) {
                //Progress
                Log.e("In CompressVideo", "In OnProgress");
//                progressTv.setText(String.valueOf(percent) + "%");
            }
        });
    }*/

    public void askQueryApiCall() {
        if (Config.isConnectedToInternet(getActivity())) {
            pd = new ProgressDialog(getActivity());
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();

            RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
            Call<Common> call = null;
            if (video == null && attachmentList.size() == 0) {
                Log.e("In FrgmntQuery", "In If : Both video and attachments are not selected");
                call = apiInterface.askQueryWithoutMultipart(title, query,
                        Config.getSharedPreferences(getActivity(), "userId"), expertId + "");
            } else {
                Log.e("In FrgmntQuery", "In Else : Either of video and attachments are selected");
                call = apiInterface.askQuery(createPartFromString(title), createPartFromString(query),
                        createPartFromString(Config.getSharedPreferences(getActivity(), "userId")),
                        createPartFromString("" + expertId), video, attachmentList,
                        createPartFromString("" + attachmentList.size()));
            }

            call.enqueue(new Callback<Common>() {
                @Override
                public void onResponse(Call<Common> call, Response<Common> response) {
                    pd.dismiss();
                    Common ac = response.body();
                    Log.e("In FrgmntQuery", "Response : " + new Gson().toJson(ac));
                    if (ac.getCode() == 100) {
//                                    Config.showDialog(getActivity(), "Your query has been added successfully");
                        final DialogPlus dialog = DialogPlus.newDialog(getActivity())
                                .setContentHolder(new ViewHolder(R.layout.dialog_ok_layout))
                                .setContentHeight(LinearLayout.LayoutParams.WRAP_CONTENT)
                                .setGravity(Gravity.CENTER)
                                .create();
                        dialog.show();

                        View errorView = dialog.getHolderView();
                        TextView titleTv = (TextView) errorView.findViewById(R.id.titleTv);
                        TextView messageTv = (TextView) errorView.findViewById(R.id.messageTv);
                        Button okBtn = (Button) errorView.findViewById(R.id.okBtn);
                        titleTv.setVisibility(View.GONE);
                        messageTv.setText("Your query has been added successfully");
                        okBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                resetAllViews();
                            }
                        });
                    } else {
                        Config.showDialog(getActivity(), ac.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<Common> call, Throwable t) {
                    pd.dismiss();
                    Log.e("In FrgmntQuery", "In OnFailure Excp :" + t.getMessage());
                    Config.showDialog(getActivity(), Config.FailureMsg);
                }
            });
        } else {
            Config.showAlertForInternet(getActivity());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        getActivity().onBackPressed();
        return true;
    }

    public boolean onBackPressed() {
        if (queryVideoView != null && queryVideoView.isFullscreen()) {
            queryVideoView.setFullscreen(false);
            return false;
        }
        return true;
    }

}
