package com.accreteit.moneylife.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Expert {
    @SerializedName("expert_id")
    @Expose
    private Integer id;
    @SerializedName("num_of_answers")
    @Expose
    private Integer numOfAnswers;
    @SerializedName("expert_name")
    @Expose
    private String name;
    @SerializedName("expert_image")
    @Expose
    private String image;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("questions")
    @Expose
    private List<Question> questions = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumOfAnswers() {
        return numOfAnswers;
    }

    public void setNumOfAnswers(Integer numOfAnswers) {
        this.numOfAnswers = numOfAnswers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
