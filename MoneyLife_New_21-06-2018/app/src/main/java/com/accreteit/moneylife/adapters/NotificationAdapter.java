package com.accreteit.moneylife.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.accreteit.moneylife.Config;
import com.accreteit.moneylife.QuestionDetail;
import com.accreteit.moneylife.R;
import com.accreteit.moneylife.models.Notification;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;


import java.util.List;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    List<Notification> data;
    private LayoutInflater inflater;
    private Context context;

    public NotificationAdapter(Context context, List<Notification> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_notification_list, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                Notification n = data.get(position);
                Intent intent = new Intent(context, QuestionDetail.class);
                intent.putExtra("questionId", n.getQueryId() + "");
                context.startActivity(intent);
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Notification n = data.get(position);
//        Glide.with(context)
//                .load(n.getImage())
//                .apply(new RequestOptions().circleCrop()
//                        .placeholder(R.drawable.image_loader)
//                        .diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .skipMemoryCache(true))
//                .into(holder.imageIv);
//        holder.nameTv.setText(n.getName());
        holder.descriptionTv.setText(n.getDescription());
        holder.timeTv.setText(n.getTime());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageIv;
        TextView nameTv, descriptionTv, timeTv;

        public MyViewHolder(View itemView) {
            super(itemView);
//            imageIv = (ImageView) itemView.findViewById(R.id.imageIv);
//            nameTv = (TextView) itemView.findViewById(R.id.nameTv);
            descriptionTv = (TextView) itemView.findViewById(R.id.descriptionTv);
            timeTv = (TextView) itemView.findViewById(R.id.timeTv);
        }
    }
}
