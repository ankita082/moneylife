package com.accreteit.moneylife.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.accreteit.moneylife.Config;
import com.accreteit.moneylife.QuestionDetail;
import com.accreteit.moneylife.QuestionsList;
import com.accreteit.moneylife.R;
import com.accreteit.moneylife.expert.activity.ExpertDetail;
import com.accreteit.moneylife.expert.activity.ReplyActivity;
import com.accreteit.moneylife.models.Category;
import com.accreteit.moneylife.models.Question;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;


public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.MyViewHolder> implements Filterable {
    List<Question> data, filteredData;
    private LayoutInflater inflater;
    private Context context;
    SearchFilter searchFilter;

    public QuestionsAdapter(Context context, List<Question> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.filteredData = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_questions_list, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                Question q = data.get(position);
//                if (Config.getSharedPreferences(context, "userType").equals("1")) {
                    // Note :- For user --> userType -> 1
                    Intent intent = new Intent(context, QuestionDetail.class);
                    intent.putExtra("questionId", "" + q.getQuestionId());
                    context.startActivity(intent);
//                } else if (Config.getSharedPreferences(context, "userType").equals("2")) {
//                    // Note :- For expert --> userType -> 2
//                    if (q.getIsAnswered() == 1) {
//                        Intent intent = new Intent(context, QuestionDetail.class);
//                        intent.putExtra("questionId", "" + q.getQuestionId());
//                        context.startActivity(intent);
//                    } else if (q.getIsAnswered() == 0) {
//                        Intent intent = new Intent(context, ReplyActivity.class);
//                        context.startActivity(intent);
//                    }
//                }
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Question q = data.get(position);

        if (Config.getSharedPreferences(context, "userType").equals("1")) {
            if(context instanceof ExpertDetail){
                // Note :- For Expert Detail screen, when the user clicks on expert and gets all the questions asked to expert
//                Glide.with(context)
//                        .load(q.getUserImage())
//                        .apply(new RequestOptions().circleCrop()
//                                .placeholder(R.drawable.image_loader)
//                                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                                .skipMemoryCache(true))
//                        .into(holder.imageIv);
                holder.expertTv.setText("Questioned By : ");
                holder.expertNameTv.setText(q.getUserName());
            } else {
//                Glide.with(context)
//                        .load(q.getExpertImage())
//                        .apply(new RequestOptions().circleCrop()
//                                .placeholder(R.drawable.image_loader)
//                                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                                .skipMemoryCache(true))
//                        .into(holder.imageIv);
                holder.expertTv.setText("Asked To : ");
                holder.expertNameTv.setText(q.getExpertName());
            }
        } else if (Config.getSharedPreferences(context, "userType").equals("2")) {
//            Glide.with(context)
//                    .load(q.getUserImage())
//                    .apply(new RequestOptions().circleCrop()
//                            .placeholder(R.drawable.image_loader)
//                            .diskCacheStrategy(DiskCacheStrategy.NONE)
//                            .skipMemoryCache(true))
//                    .into(holder.imageIv);
            holder.expertTv.setText("Questioned By : ");
            holder.expertNameTv.setText(q.getUserName());
        }
        Glide.with(context)
                .load(q.getUserImage())
                .apply(new RequestOptions().circleCrop()
                        .placeholder(R.drawable.image_loader)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true))
                .into(holder.imageIv);

        holder.questionTv.setText(q.getQuestionTitle());

//        if (q.getQuestionDescription().length() > 90) {
//            holder.questionDescriptionTv.setText(q.getQuestionDescription().substring(0, 90) + "...");
//        } else {
        holder.questionDescriptionTv.setText(q.getQuestionDescription());
//        }
        holder.dateTimeTv.setText(q.getDateTime());
        holder.commentsTv.setText(q.getCommentsNumber() + " Comments");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public Filter getFilter() {
        if (searchFilter == null)
            searchFilter = new SearchFilter();
        return searchFilter;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageIv;
        TextView questionTv, expertTv, expertNameTv, commentsTv, questionDescriptionTv, dateTimeTv;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageIv = itemView.findViewById(R.id.imageIv);
            questionTv = (TextView) itemView.findViewById(R.id.questionTv);
            expertTv = itemView.findViewById(R.id.expertTv);
            expertNameTv = (TextView) itemView.findViewById(R.id.expertNameTv);
            commentsTv = (TextView) itemView.findViewById(R.id.commentsTv);
            dateTimeTv = (TextView) itemView.findViewById(R.id.dateTimeTv);
            questionDescriptionTv = (TextView) itemView.findViewById(R.id.questionDescriptionTv);
        }
    }

    public class SearchFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();
            if (charSequence == null || charSequence.length() == 0) {
                results.values = filteredData;
                results.count = filteredData.size();
            } else {
                // We perform filtering operation
                List<Question> nList = new ArrayList<Question>();

                for (Question q : filteredData) {
                    if (q.getQuestionTitle().toLowerCase().contains(charSequence.toString().toLowerCase()) ||
                            q.getQuestionDescription().toLowerCase().contains(charSequence.toString().toLowerCase()) ||
                            q.getUserName().toLowerCase().contains(charSequence.toString().toLowerCase()) ||
                            q.getDateTime().toLowerCase().contains(charSequence.toString().toLowerCase()))
                        nList.add(q);
                }

                results.values = nList;
                results.count = nList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            data = (List<Question>) filterResults.values;
            notifyDataSetChanged();
        }
    }
}
