package com.accreteit.moneylife;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.accreteit.moneylife.models.UserInfo;
import com.accreteit.moneylife.responses.Common;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.accreteit.moneylife.RetroApiClient.okHttpClient;

public class MyProfile extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolbarTitle;
    ImageView backIv;
    LinearLayout editLayout, changePictureLayout;
    TextInputLayout nameInput, emailInput, contactInput;
    EditText nameEt, emailEt, contactEt;
    TextView updateBtn;
    ImageView imageIv;
    Boolean isValid;
    ProgressDialog pd;
    UserInfo ui;
    int count = -1;
    String userType = "", userChosenTask = "", isImage = "", isUpdate = "";
    public int REQUEST_CODE = 124;
    int REQUEST_CAMERA = 100, REQUEST_GALLERY = 101;
    File imageFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        initToolbar();

        editLayout = findViewById(R.id.editLayout);
        changePictureLayout = findViewById(R.id.changePictureLayout);
        nameInput = findViewById(R.id.nameInput);
        emailInput = findViewById(R.id.emailInput);
        contactInput = findViewById(R.id.contactInput);
        nameEt = findViewById(R.id.nameEt);
        emailEt = findViewById(R.id.emailEt);
        contactEt = findViewById(R.id.contactEt);
        updateBtn = findViewById(R.id.updateBtn);
        imageIv = findViewById(R.id.imageIv);

        // Note :- Contact number is not stored for expert
        userType = Config.getSharedPreferences(MyProfile.this, "userType");
        if (userType.equals("1"))
            contactInput.setVisibility(View.VISIBLE);
        else if (userType.equals("2"))
            contactInput.setVisibility(View.GONE);

//        changeEnabledStatus();

        if (userType.equals("1"))
            getProfile();
        else if (userType.equals("2"))
            getProfileForExpert();

//        editLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ++count;
//                if (count % 2 == 0) {
//                    changeEnabledStatus(true);
//                } else {
//                    changeEnabledStatus(false);
//                }
//            }
//        });

        changePictureLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(MyProfile.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(MyProfile.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(MyProfile.this, android.Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MyProfile.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                                    android.Manifest.permission.CAMERA}, REQUEST_CODE);
                } else {
                    selectImage();
                }

            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (count % 2 == 0) {
                Config.hideSoftKeyboard(MyProfile.this);
                isValid = true;
                nameInput.setErrorEnabled(false);
                contactInput.setErrorEnabled(false);
                if (nameEt.getText().toString().equalsIgnoreCase("")) {
                    isValid = false;
                    nameInput.setErrorEnabled(true);
                    nameInput.setError("Must enter full name");
                }
                if (userType.equals("1")) {
                    if (contactEt.getText().toString().equalsIgnoreCase("")) {
                        isValid = false;
                        contactInput.setErrorEnabled(true);
                        contactInput.setError("Must enter contact number");
                    } else {
                        if (contactEt.getText().toString().length() != 10) {
                            isValid = false;
                            contactInput.setErrorEnabled(true);
                            contactInput.setError("Contact number must contain 10 digits");
                        }
                    }
                }

                if (isValid) {
                    if (userType.equals("1"))
                        updateProfile();
                    else if (userType.equals("2"))
                        updateProfileForExpert();
                }
//                } else {
//                    Config.showDialog(MyProfile.this, "Please click on edit to update profile");
//                }
            }
        });

    }

    public void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("My Profile");
        backIv = toolbar.findViewById(R.id.backIv);

        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void changeEnabledStatus() {
        emailEt.setEnabled(false);
        nameEt.setEnabled(true);
        contactEt.setEnabled(true);
    }

    public void getProfile() {
        if (Config.isConnectedToInternet(MyProfile.this)) {
            pd = new ProgressDialog(MyProfile.this);
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            if (isUpdate.equals(""))
                pd.show();

            String userId = Config.getSharedPreferences(MyProfile.this, "userId");
            String email = Config.getSharedPreferences(MyProfile.this, "email");
            Log.e("In MyProfile", "Params : UserId -> " + userId + ", Email -> " + email);
            Retrofit retro = new Retrofit.Builder()
                    .baseUrl(Config.SignUpnLoginUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            RetroApiInterface apiInterface = retro.create(RetroApiInterface.class);
            Call<Common> call = apiInterface.getProfile(email, userId);
            call.enqueue(new Callback<Common>() {
                @Override
                public void onResponse(Call<Common> call, Response<Common> response) {
                    if (pd.isShowing())
                        pd.dismiss();
                    Common c = response.body();
                    Log.e("In MyProfile", "Response : " + new Gson().toJson(c));
                    ui = c.getUserInfo();
                    if (isUpdate.equals("")) {
                        if (ui != null && c != null && c.getCode() == 100) {
                            emailEt.setText(ui.getEmailId());
                            nameEt.setText(ui.getFullName());
                            contactEt.setText(ui.getMobileNo());
                            if (ui.getProfilePicture() != null && !ui.getProfilePicture().equals("")) {
                                Glide.with(MyProfile.this)
                                        .load(ui.getProfilePicture())
                                        .apply(new RequestOptions()
                                                .placeholder(R.drawable.image_loader)
                                                .error(R.drawable.ic_launcher)
                                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                .skipMemoryCache(true))
                                        .into(imageIv);
                            }

                        } else {
                            Config.showDialog(MyProfile.this, c.getMessage());
                        }
                    } else if (isUpdate.equals("1")) {
                        // Note :- Api call for sending updated data to server
                        sendUserDataToDb(ui);
                    }
                }

                @Override
                public void onFailure(Call<Common> call, Throwable t) {
                    if (pd.isShowing())
                        pd.dismiss();
                    Log.e("In MyProfile", "GetUserProfile In OnFailure Msg : " + t.getMessage());
                    Config.showDialog(MyProfile.this, Config.FailureMsg);
                }
            });

        } else {
            Config.showAlertForInternet(MyProfile.this);
        }
    }

    public void sendUserDataToDb(UserInfo ui) {
        String userId = Config.getSharedPreferences(MyProfile.this, "userId");
        String token = Config.getSharedPreferences(MyProfile.this, "token");
        RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
        Call<Common> call = apiInterface.addUserToDb(ui.getEmailId(), ui.getFullName(), ui.getProfilePicture(), userId, token,
                "0");
        call.enqueue(new Callback<Common>() {
            @Override
            public void onResponse(Call<Common> call, Response<Common> response) {
                Common c = response.body();
                Log.e("In MyProfile", "In SenDataToDb Response : " + new Gson().toJson(c));
            }

            @Override
            public void onFailure(Call<Common> call, Throwable t) {
                Log.e("In MyProfile", "In SenDataToDb OnFailure Excp : " + t.getMessage());
            }
        });

    }

    public void updateProfile() {
        if (Config.isConnectedToInternet(MyProfile.this)) {
            pd = new ProgressDialog(MyProfile.this);
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();

            Retrofit retro = new Retrofit.Builder()
                    .baseUrl(Config.SignUpnLoginUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            MultipartBody.Part userImage = null;
            if (imageFile != null)
                userImage = prepareFilePart("profile_photo");
            RetroApiInterface apiInterface = retro.create(RetroApiInterface.class);
            Call<Common> call = null;
            if (isImage.equalsIgnoreCase("1"))
                call = apiInterface.updateProfileWithMultipart(ui.getEmailId(),
                        Config.getSharedPreferences(MyProfile.this, "userId"), ui.getUserName(),
                        nameEt.getText().toString(), contactEt.getText().toString(), userImage);
            else
                call = apiInterface.updateProfileWithoutMultipart(ui.getEmailId(),
                        Config.getSharedPreferences(MyProfile.this, "userId"), ui.getUserName(),
                        nameEt.getText().toString(), contactEt.getText().toString());
            call.enqueue(new Callback<Common>() {
                @Override
                public void onResponse(Call<Common> call, Response<Common> response) {
                    pd.dismiss();
                    Common c = response.body();
                    if (c != null && c.getCode() == 100) {
                        isUpdate = "1";
//                          Config.showDialog(MyProfile.this, c.getMessage());
                        final DialogPlus dialog = DialogPlus.newDialog(MyProfile.this)
                                .setContentHolder(new ViewHolder(R.layout.dialog_ok_layout))
                                .setContentHeight(LinearLayout.LayoutParams.WRAP_CONTENT)
                                .setGravity(Gravity.CENTER)
                                .create();
                        dialog.show();

                        View errorView = dialog.getHolderView();
                        TextView titleTv = (TextView) errorView.findViewById(R.id.titleTv);
                        TextView messageTv = (TextView) errorView.findViewById(R.id.messageTv);
                        Button okBtn = (Button) errorView.findViewById(R.id.okBtn);
                        titleTv.setVisibility(View.GONE);
                        messageTv.setText(c.getMessage());
                        okBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                // Note :- Call For updated user data
                                getProfile();
                            }
                        });
                    } else {
                        Config.showDialog(MyProfile.this, c.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<Common> call, Throwable t) {
                    pd.dismiss();
                    Log.e("In MyProfile", "UpdateUserProfile In OnFailure Excp : " + t.getMessage());
                    Config.showDialog(MyProfile.this, Config.FailureMsg);
                }
            });
        } else {
            Config.showAlertForInternet(MyProfile.this);
        }
    }

    public void getProfileForExpert() {
        pd = new ProgressDialog(MyProfile.this);
        pd.setTitle("Please wait");
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        pd.show();

        String userId = Config.getSharedPreferences(MyProfile.this, "userId");
        RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
        Call<Common> call = apiInterface.getProfileForExpert(userId);
        call.enqueue(new Callback<Common>() {
            @Override
            public void onResponse(Call<Common> call, Response<Common> response) {
                pd.dismiss();
                Common c = response.body();
                if (c != null && c.getCode() == 100) {
                    emailEt.setText(c.getExpertEmail());
                    nameEt.setText(c.getExpertName());
                    if (c.getExpertImage() != null && !c.getExpertImage().equals("")) {
                        Glide.with(MyProfile.this)
                                .load(c.getExpertImage())
                                .apply(new RequestOptions()
                                        .placeholder(R.drawable.image_loader)
                                        .error(R.drawable.ic_launcher)
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true))
                                .into(imageIv);
                    } else {
                        Config.showDialog(MyProfile.this, c.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<Common> call, Throwable t) {
                pd.dismiss();
                Log.e("In MyProfile", "GetExpertProfile In OnFailure Msg : " + t.getMessage());
                Config.showDialog(MyProfile.this, Config.FailureMsg);
            }
        });
    }

    public void updateProfileForExpert() {
        if (Config.isConnectedToInternet(MyProfile.this)) {
            pd = new ProgressDialog(MyProfile.this);
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();

            String userId = Config.getSharedPreferences(MyProfile.this, "userId");
            String email = Config.getSharedPreferences(MyProfile.this, "email");
            MultipartBody.Part expertImage = null;
            if (imageFile != null)
                expertImage = prepareFilePart("expert_image");
            RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
            Call<Common> call = null;
            if (isImage.equalsIgnoreCase("1"))
                call = apiInterface.updateProfileWithMultipartForExpert(createPartFromString(userId), createPartFromString(email),
                        createPartFromString(nameEt.getText().toString()), expertImage);
            else
                call = apiInterface.updateProfileWithoutMultipartForExpert(createPartFromString(userId), createPartFromString(email),
                        createPartFromString(nameEt.getText().toString()));
            call.enqueue(new Callback<Common>() {
                @Override
                public void onResponse(Call<Common> call, Response<Common> response) {
                    pd.dismiss();
                    Common c = response.body();
                    if (c != null && c.getCode() == 100) {
                        Config.showDialog(MyProfile.this, c.getMessage());
                    } else {
                        Config.showDialog(MyProfile.this, c.getMessage());
                    }

                }

                @Override
                public void onFailure(Call<Common> call, Throwable t) {
                    pd.dismiss();
                    Log.e("In MyProfile", "UpdateExpertProfile In OnFailure Msg : " + t.getMessage());
                    Config.showDialog(MyProfile.this, Config.FailureMsg);
                }
            });

        } else {
            Config.showAlertForInternet(MyProfile.this);
        }
    }

    private void selectImage() {
        final CharSequence[] items = {"Camera", "Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MyProfile.this);
        builder.setTitle("Add Photo!");
        builder.setCancelable(false);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = true;
                if (items[item].equals("Camera")) {
                    userChosenTask = "Camera";
                    cameraIntent();
                } else if (items[item].equals("Gallery")) {
                    userChosenTask = "Gallery";
                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(intent, "Select File"), REQUEST_GALLERY);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Bitmap photo = null;
            if (requestCode == REQUEST_GALLERY) {
                if (data != null) {
                    try {
                        photo = MediaStore.Images.Media.getBitmap(MyProfile.this.getContentResolver(), data.getData());
                    } catch (IOException e) {
                        Log.e("In MyProfile", "IO Excp Msg : " + e.getMessage());
                    }
                }
                imageFile = new File(data.getData().toString());
            } else if (requestCode == REQUEST_CAMERA) {
                photo = (Bitmap) data.getExtras().get("data");
            }
            imageIv.setImageBitmap(photo);
            String imageStoragePath = Environment.getExternalStorageDirectory() + "/MoneyLife";
            File imageStorageDir = new File(imageStoragePath);
            if (!imageStorageDir.exists()) {
                imageStorageDir.mkdirs();
            }
            try {
                isImage = "1";
                String userId = Config.getSharedPreferences(MyProfile.this, "userId");
                imageFile = new File(imageStorageDir.toString() + File.separator + userId + ".jpg");
                if (imageFile.exists()) {
                    imageFile.delete();
                }
                imageFile.createNewFile();

                FileOutputStream fileOutputStream = new FileOutputStream(imageFile);
                BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);
                photo.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                bos.flush();
                bos.close();
                Log.e("In MyProfile", "OnActResult In File(Ex-Storage) Path : " + imageFile.toString());
            } catch (FileNotFoundException e) {
                Log.e("In MyProfile", "Excp Error saving image file: " + e.getMessage());
            } catch (IOException e) {
                Log.e("In MyProfile", "Excp Error saving image file: " + e.getMessage());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                }
            }
        } else {
            ActivityCompat.requestPermissions(MyProfile.this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.CAMERA}, REQUEST_CODE);
        }
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName) {
        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(imageFile.getPath()),
                        imageFile
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, imageFile.getName(), requestFile);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

}
