package com.accreteit.moneylife.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.accreteit.moneylife.ChangePassword;
import com.accreteit.moneylife.Config;
import com.accreteit.moneylife.LoginActivity;
import com.accreteit.moneylife.MainActivity;
import com.accreteit.moneylife.MyProfile;
import com.accreteit.moneylife.MyQuestions;
import com.accreteit.moneylife.R;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

public class FragmentMyAccount extends Fragment {

    TextView toolbarTitle;
    ImageView backIv;
    LinearLayout profileLayout, questionsLayout, changePasswordLayout, logoutLayout;

    public FragmentMyAccount() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_account, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        setHasOptionsMenu(true);

        initToolbar();

        profileLayout = rootView.findViewById(R.id.profileLayout);
        questionsLayout = rootView.findViewById(R.id.questionsLayout);
        changePasswordLayout = rootView.findViewById(R.id.changePasswordLayout);
        logoutLayout = rootView.findViewById(R.id.logoutLayout);

        if (Config.getSharedPreferences(getActivity(), "userType").equals("1"))
            questionsLayout.setVisibility(View.VISIBLE);
        else if (Config.getSharedPreferences(getActivity(), "userType").equals("2"))
            questionsLayout.setVisibility(View.GONE);

        profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MyProfile.class);
                getActivity().startActivity(intent);
            }
        });

        changePasswordLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ChangePassword.class);
                getActivity().startActivity(intent);
            }
        });

        questionsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MyQuestions.class);
                getActivity().startActivity(intent);
            }
        });

        logoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DialogPlus dialog = DialogPlus.newDialog(getActivity())
                        .setContentHolder(new ViewHolder(R.layout.dialog_confirmation_layout))
                        .setCancelable(false)
                        .setGravity(Gravity.CENTER)
                        .create();
                dialog.show();

                View dialogView = dialog.getHolderView();
                TextView titleTv = dialogView.findViewById(R.id.titleTv);
                Button yesBtn = dialogView.findViewById(R.id.yesBtn);
                Button noBtn = dialogView.findViewById(R.id.noBtn);
                titleTv.setText("Are you sure you want to logout ? ");

                yesBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Config.saveSharedPreferences(getActivity(), "userId", null);
                        Config.saveSharedPreferences(getActivity(), "userType", null);
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

                noBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });

        return rootView;
    }

    public void initToolbar() {
        toolbarTitle = ((MainActivity) getActivity()).toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("My Account");
        backIv = ((MainActivity) getActivity()).toolbar.findViewById(R.id.backIv);

        backIv.setImageResource(R.drawable.ic_toolbar_logo);
//        backIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getActivity().onBackPressed();
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        getActivity().onBackPressed();
        return true;
    }

}
