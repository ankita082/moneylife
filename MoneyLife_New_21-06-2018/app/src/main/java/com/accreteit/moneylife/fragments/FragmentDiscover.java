package com.accreteit.moneylife.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.accreteit.moneylife.Config;
import com.accreteit.moneylife.MainActivity;
import com.accreteit.moneylife.R;
import com.accreteit.moneylife.RetroApiClient;
import com.accreteit.moneylife.RetroApiInterface;
import com.accreteit.moneylife.adapters.CategoryAdapter;
import com.accreteit.moneylife.responses.Common;
import com.accreteit.moneylife.responses.GetAllCategories;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDiscover extends Fragment {

    TextView toolbarTitle;
    ImageView backIv;
    RecyclerView categoryList;
    CategoryAdapter adapter;
    TextView noDataTv;
//    String responseJson = "{ \"code\": 100, \"category_list\": [ { \"category_id\": 1, \"category_title\": \"Fashion Bags\", \"category_image\": \"https://picsum.photos/458/354/?image=882\", \"experts_number\": \"10\" }, { \"category_id\": 2, \"category_title\": \"Electronic Devices\", \"category_image\": \"https://picsum.photos/458/354\", \"experts_number\": \"5\" }, { \"category_id\": 3, \"category_title\": \"Wallets\", \"category_image\": \"https://picsum.photos/1263/285/?image=933\", \"experts_number\": \"12\" }, { \"category_id\": 4, \"category_title\": \"Accessories\", \"category_image\": \"https://picsum.photos/458/354?gravity=east\", \"experts_number\": \"8\" } ] }";
//    GetAllCategories ac;
    SearchView searchView;
    ProgressDialog pd;

    public FragmentDiscover() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_discover, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        setHasOptionsMenu(true);

        initToolbar();

        categoryList = rootView.findViewById(R.id.categoryList);
        noDataTv = rootView.findViewById(R.id.noDataTv);
        categoryList.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (Config.isConnectedToInternet(getActivity())) {
            pd = new ProgressDialog(getActivity());
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();

            RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
            Call<GetAllCategories> call = apiInterface.getCategories();
            call.enqueue(new Callback<GetAllCategories>() {
                @Override
                public void onResponse(Call<GetAllCategories> call, Response<GetAllCategories> response) {
                    pd.dismiss();
                    GetAllCategories ac = response.body();
                    if (ac.getCategoryList().size() == 0) {
                        noDataTv.setVisibility(View.VISIBLE);
                        categoryList.setVisibility(View.GONE);
                    } else {
                        noDataTv.setVisibility(View.GONE);
                        categoryList.setVisibility(View.VISIBLE);
                        adapter = new CategoryAdapter(getActivity(), ac.getCategoryList());
                        categoryList.setAdapter(adapter);
                    }
                }

                @Override
                public void onFailure(Call<GetAllCategories> call, Throwable t) {
                    pd.dismiss();
                    Config.showDialog(getActivity(), Config.FailureMsg);
                }
            });
        } else {
            Config.showAlertForInternet(getActivity());
        }


        return rootView;
    }

    public void initToolbar() {
        toolbarTitle = ((MainActivity) getActivity()).toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Discover");
        backIv = ((MainActivity) getActivity()).toolbar.findViewById(R.id.backIv);

        backIv.setImageResource(R.drawable.ic_toolbar_logo);
//        backIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getActivity().onBackPressed();
//            }
//        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        getActivity().getMenuInflater().inflate(R.menu.menu_home, menu);

        final MenuItem searchMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        int searchImgId = android.support.v7.appcompat.R.id.search_button; // I used the explicit layout ID of searchview's ImageView
        ImageView v = (ImageView) searchView.findViewById(searchImgId);
        v.setImageResource(R.drawable.ic_search);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.e("In FrgmtDiscover", "OnQuerySubmit : " + query);
//                if( ! searchView.isIconified()) {
//                    searchView.setIconified(true);
//                }
                searchMenuItem.collapseActionView();
                adapter.getFilter().filter(query);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                Log.e("In FrgmtDiscover", "OnQueryChange : " + s);
                adapter.getFilter().filter(s);
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_search) {
        } else {
//            getActivity().onBackPressed();
        }
        return true;
    }

}
