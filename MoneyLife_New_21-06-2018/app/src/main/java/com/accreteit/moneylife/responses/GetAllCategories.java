package com.accreteit.moneylife.responses;

import com.accreteit.moneylife.models.Category;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 22-Mar-18.
 */

public class GetAllCategories {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("category")
    @Expose
    private List<Category> categoryList = null;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
}
