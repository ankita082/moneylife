package com.accreteit.moneylife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.accreteit.moneylife.adapters.CategoryAdapter;
import com.accreteit.moneylife.adapters.QuestionsAdapter;
import com.accreteit.moneylife.models.Category;
import com.accreteit.moneylife.responses.GetAllCategories;
import com.accreteit.moneylife.responses.GetQuestions;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionsList extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolbarTitle, noDataTv;
    ImageView backIv;
    RecyclerView recyclerView;
    QuestionsAdapter adapter;
    String categoryId = "", categoryName = "";
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions_list);

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("categoryId")) {
            categoryId = intent.getStringExtra("categoryId");
            categoryName = intent.getStringExtra("categoryName");
        }

        initToolbar();
        recyclerView = findViewById(R.id.recyclerview);
        noDataTv = findViewById(R.id.noDataTv);
        recyclerView.setLayoutManager(new LinearLayoutManager(QuestionsList.this));

        if (Config.isConnectedToInternet(QuestionsList.this)) {
            pd = new ProgressDialog(QuestionsList.this);
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();

            RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
            Call<GetQuestions> call = apiInterface.getAllQuestions(categoryId);
            call.enqueue(new Callback<GetQuestions>() {
                @Override
                public void onResponse(Call<GetQuestions> call, Response<GetQuestions> response) {
                    pd.dismiss();
                    GetQuestions ac = response.body();

                    if (ac.getQuestionList().size() > 0) {
                        noDataTv.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        adapter = new QuestionsAdapter(QuestionsList.this, ac.getQuestionList());
                        recyclerView.setAdapter(adapter);
                    } else {
                        noDataTv.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<GetQuestions> call, Throwable t) {
                    pd.dismiss();
                    Config.showDialog(QuestionsList.this, Config.FailureMsg);
                }
            });
        } else {
            Config.showAlertForInternet(QuestionsList.this);
        }
    }

    public void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText(categoryName);
        backIv = toolbar.findViewById(R.id.backIv);
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_search) {
        } else {
            onBackPressed();
        }
        return true;
    }


}
