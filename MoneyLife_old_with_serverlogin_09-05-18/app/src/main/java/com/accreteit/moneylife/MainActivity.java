package com.accreteit.moneylife;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.accreteit.moneylife.expert.fragments.FragmentMyRequestTabs;
import com.accreteit.moneylife.fragments.FragmentDiscover;
import com.accreteit.moneylife.fragments.FragmentExperts;
import com.accreteit.moneylife.fragments.FragmentMyAccount;
import com.accreteit.moneylife.fragments.FragmentNotification;
import com.accreteit.moneylife.fragments.FragmentQuery;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public Toolbar toolbar;
    BottomNavigationViewEx navigationView;
    Fragment fragment;
    LinearLayout footer, bottomLayout;
    int lastPos = 2, selPos = 2;
//    private boolean firstClick = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        navigationView = findViewById(R.id.navigationView);
        footer = findViewById(R.id.footer);
        bottomLayout = findViewById(R.id.bottomLayout);

        // Note :- UserType values --> 1 -> User and 2 -> Expert

        /* // For Expert --> Custom Data
        Config.saveSharedPreferences(MainActivity.this, "userId", "2"); // For testing purpose
        Config.saveSharedPreferences(MainActivity.this, "userType", "2"); // For testing purpose  */
        /*  // For User --> Custom Data
        Config.saveSharedPreferences(MainActivity.this, "userId", "1"); // For testing purpose
        Config.saveSharedPreferences(MainActivity.this, "userType", "1"); // For testing purpose  */

        if (Config.getSharedPreferences(MainActivity.this, "userType").equals("1")) {
//            navigationView.inflateMenu(R.menu.navigation_user);
////        Note :- Below is the code for center icon only
//            int centerPosition = 2;
//            navigationView.setIconSizeAt(centerPosition, 40, 40);
//            navigationView.setItemBackground(centerPosition, R.color.white);
//            navigationView.getMenu().getItem(centerPosition).setIcon(R.drawable.ic_plus_black);
////            navigationView.setIconTintList(centerPosition, getResources().getColorStateList(R.color.white));
//            loadFragment("query");

//            Note :- Included custom bottom layout for user as center icon is not displayed properly
            footer.setVisibility(View.VISIBLE);
            navigationView.setVisibility(View.GONE);
            bottomLayout = footer.findViewById(R.id.bottomLayout);
            bottomLayout.findViewById(R.id.discoverLayout).setTag("discover");
            bottomLayout.findViewById(R.id.notificationLayout).setTag("notification");
            bottomLayout.findViewById(R.id.queryIv).setTag("query");
            bottomLayout.findViewById(R.id.expertLayout).setTag("expert");
            bottomLayout.findViewById(R.id.myAccountLayout).setTag("myaccount");
            bottomLayout.findViewById(R.id.discoverLayout).setOnClickListener(this);
            bottomLayout.findViewById(R.id.notificationLayout).setOnClickListener(this);
            bottomLayout.findViewById(R.id.queryIv).setOnClickListener(this);
            bottomLayout.findViewById(R.id.expertLayout).setOnClickListener(this);
            bottomLayout.findViewById(R.id.myAccountLayout).setOnClickListener(this);
            loadFragment("query");

        } else if (Config.getSharedPreferences(MainActivity.this, "userType").equals("2")) {
            footer.setVisibility(View.GONE);
            navigationView.setVisibility(View.VISIBLE);
            navigationView.inflateMenu(R.menu.navigation_expert);
            loadFragment("myrequests");

            navigationView.enableAnimation(false);
            navigationView.enableShiftingMode(false);
            navigationView.enableItemShiftingMode(false);
            navigationView.setCurrentItem(2);
            navigationView.setTextSize(10);
            navigationView.setIconTintList(2, getResources()
                    .getColorStateList(R.color.cinnabar));
            navigationView.setTextTintList(2, getResources()
                    .getColorStateList(R.color.cinnabar));
        }


        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                Note :- Below is the logic for icon and text selector in navigation view
                int selPos = navigationView.getMenuItemPosition(item);
                if (selPos != lastPos) {
                    navigationView.setIconTintList(selPos, getResources()
                            .getColorStateList(R.color.cinnabar));
                    navigationView.setTextTintList(selPos, getResources()
                            .getColorStateList(R.color.cinnabar));
                    navigationView.setIconTintList(lastPos, getResources()
                            .getColorStateList(R.color.darkerGrey));
                    navigationView.setTextTintList(lastPos, getResources()
                            .getColorStateList(R.color.darkerGrey));
                    Log.e("In OnItemSelect", "SelPos n LstPos : " + selPos + " n " + lastPos);
                }
                lastPos = selPos;

                /* Previously used when no bottom custom bar was there for user
                if (Config.getSharedPreferences(MainActivity.this, "userType").equals("1")) {
                    switch (item.getItemId()) {
                        case R.id.bottom_discover:
                            loadFragment("discover");
                            return true;
                        case R.id.bottom_notification:
                            loadFragment("notification");
                            return true;
                        case R.id.bottom_query:
                            loadFragment("query");
                            return true;
                        case R.id.bottom_experts:
                            loadFragment("expert");
                            return true;
                        case R.id.bottom_my_account:
                            loadFragment("myaccount");
                            return true;
                        default:
                            break;
                    }
                } else if (Config.getSharedPreferences(MainActivity.this, "userType").equals("2")) { */
                    switch (item.getItemId()) {
                        case R.id.bottom_discover:
                            loadFragment("discover");
                            return true;
                        case R.id.bottom_notification:
                            loadFragment("notification");
                            return true;
                        case R.id.bottom_my_requests:
                            loadFragment("myrequests");
                            return true;
                        case R.id.bottom_my_account:
                            loadFragment("myaccount");
                            return true;
                        default:
                            break;
                    // }
                }
                return false;
            }
        });


    }

    @Override
    public void onClick(View view) {
//        Log.e("In MainActivity", "In OnClick : " + view.getTag().toString());
        String tag = view.getTag().toString();
        if (tag.equalsIgnoreCase("discover")) {
            ((ImageView) bottomLayout.findViewById(R.id.discoverIv)).setImageResource(R.drawable.ic_discover_selected);
            ((ImageView) bottomLayout.findViewById(R.id.notificationIv)).setImageResource(R.drawable.ic_notification);
            ((ImageView) bottomLayout.findViewById(R.id.queryIv)).setImageResource(R.drawable.ic_plus_black);
            ((ImageView) bottomLayout.findViewById(R.id.expertIv)).setImageResource(R.drawable.ic_expert);
            ((ImageView) bottomLayout.findViewById(R.id.myAccountIv)).setImageResource(R.drawable.ic_my_account);
            ((TextView) bottomLayout.findViewById(R.id.discoverTv)).setTextColor(getResources().getColor(R.color.cinnabar));
            ((TextView) bottomLayout.findViewById(R.id.notificationTv)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) bottomLayout.findViewById(R.id.expertTv)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) bottomLayout.findViewById(R.id.myAccountTv)).setTextColor(getResources().getColor(R.color.black));
        } else if (tag.equalsIgnoreCase("notification")) {
            ((ImageView) bottomLayout.findViewById(R.id.discoverIv)).setImageResource(R.drawable.ic_discover);
            ((ImageView) bottomLayout.findViewById(R.id.notificationIv)).setImageResource(R.drawable.ic_notification_selected);
            ((ImageView) bottomLayout.findViewById(R.id.queryIv)).setImageResource(R.drawable.ic_plus_black);
            ((ImageView) bottomLayout.findViewById(R.id.expertIv)).setImageResource(R.drawable.ic_expert);
            ((ImageView) bottomLayout.findViewById(R.id.myAccountIv)).setImageResource(R.drawable.ic_my_account);
            ((TextView) bottomLayout.findViewById(R.id.discoverTv)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) bottomLayout.findViewById(R.id.notificationTv)).setTextColor(getResources().getColor(R.color.cinnabar));
            ((TextView) bottomLayout.findViewById(R.id.expertTv)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) bottomLayout.findViewById(R.id.myAccountTv)).setTextColor(getResources().getColor(R.color.black));
        } else if (tag.equalsIgnoreCase("query")) {
            ((ImageView) bottomLayout.findViewById(R.id.discoverIv)).setImageResource(R.drawable.ic_discover);
            ((ImageView) bottomLayout.findViewById(R.id.notificationIv)).setImageResource(R.drawable.ic_notification);
            ((ImageView) bottomLayout.findViewById(R.id.queryIv)).setImageResource(R.drawable.ic_plus_cinnabar);
            ((ImageView) bottomLayout.findViewById(R.id.expertIv)).setImageResource(R.drawable.ic_expert);
            ((ImageView) bottomLayout.findViewById(R.id.myAccountIv)).setImageResource(R.drawable.ic_my_account);
            ((TextView) bottomLayout.findViewById(R.id.discoverTv)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) bottomLayout.findViewById(R.id.notificationTv)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) bottomLayout.findViewById(R.id.expertTv)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) bottomLayout.findViewById(R.id.myAccountTv)).setTextColor(getResources().getColor(R.color.black));
        } else if (tag.equalsIgnoreCase("expert")) {
            ((ImageView) bottomLayout.findViewById(R.id.discoverIv)).setImageResource(R.drawable.ic_discover);
            ((ImageView) bottomLayout.findViewById(R.id.notificationIv)).setImageResource(R.drawable.ic_notification);
            ((ImageView) bottomLayout.findViewById(R.id.queryIv)).setImageResource(R.drawable.ic_plus_black);
            ((ImageView) bottomLayout.findViewById(R.id.expertIv)).setImageResource(R.drawable.ic_expert_selected);
            ((ImageView) bottomLayout.findViewById(R.id.myAccountIv)).setImageResource(R.drawable.ic_my_account);
            ((TextView) bottomLayout.findViewById(R.id.discoverTv)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) bottomLayout.findViewById(R.id.notificationTv)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) bottomLayout.findViewById(R.id.expertTv)).setTextColor(getResources().getColor(R.color.cinnabar));
            ((TextView) bottomLayout.findViewById(R.id.myAccountTv)).setTextColor(getResources().getColor(R.color.black));
        } else if (tag.equalsIgnoreCase("myaccount")) {
            ((ImageView) bottomLayout.findViewById(R.id.discoverIv)).setImageResource(R.drawable.ic_discover);
            ((ImageView) bottomLayout.findViewById(R.id.notificationIv)).setImageResource(R.drawable.ic_notification);
            ((ImageView) bottomLayout.findViewById(R.id.queryIv)).setImageResource(R.drawable.ic_plus_black);
            ((ImageView) bottomLayout.findViewById(R.id.expertIv)).setImageResource(R.drawable.ic_expert);
            ((ImageView) bottomLayout.findViewById(R.id.myAccountIv)).setImageResource(R.drawable.ic_my_account_selected);
            ((TextView) bottomLayout.findViewById(R.id.discoverTv)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) bottomLayout.findViewById(R.id.notificationTv)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) bottomLayout.findViewById(R.id.expertTv)).setTextColor(getResources().getColor(R.color.black));
            ((TextView) bottomLayout.findViewById(R.id.myAccountTv)).setTextColor(getResources().getColor(R.color.cinnabar));
        }
        loadFragment(tag);
    }

    public void loadFragment(String name) {
        if (name.equalsIgnoreCase("discover")) {
            fragment = new FragmentDiscover();
        } else if (name.equalsIgnoreCase("notification")) {
            fragment = new FragmentNotification();
        } else if (name.equalsIgnoreCase("query")) {
            fragment = new FragmentQuery();
        } else if (name.equalsIgnoreCase("expert")) {
            fragment = new FragmentExperts();
        } else if (name.equalsIgnoreCase("myaccount")) {
            fragment = new FragmentMyAccount();
        } else if (name.equalsIgnoreCase("myrequests")) {
            fragment = new FragmentMyRequestTabs();
        }
//        Bundle bundle = new Bundle();
//        bundle.putString("userProfileResponse", new Gson().toJson(up));
//        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        boolean handled = false;
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        for (Fragment f1 : fragmentList) {
            if (f1 instanceof FragmentQuery) {
                if (!((FragmentQuery) f1).onBackPressed()) {
                    handled = true;
                    break;
                }
            }
        }
        if (!handled) {
            int backCount = getSupportFragmentManager().getBackStackEntryCount();
            Log.e("In MainAct", "In OnBackPressed Count : " + backCount);
            if (backCount <= 1) {
                finish();
            } else {
                onBackPressed();
            }
        }


    }

}
