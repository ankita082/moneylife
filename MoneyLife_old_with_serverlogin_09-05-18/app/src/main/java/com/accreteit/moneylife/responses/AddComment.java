package com.accreteit.moneylife.responses;

import com.accreteit.moneylife.models.Comment;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 04-May-18.
 */

public class AddComment {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("comments")
    @Expose
    private List<Comment> comments = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
