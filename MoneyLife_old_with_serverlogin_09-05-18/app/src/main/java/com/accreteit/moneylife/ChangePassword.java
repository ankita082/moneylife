package com.accreteit.moneylife;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ChangePassword extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolbarTitle;
    ImageView backIv;
    LinearLayout editLayout;
    TextInputLayout oldPasswordLayout, newPasswordLayout, confirmPasswordLayout;
    EditText confirmPasswordEt, newPasswordEt, oldPasswordEt;
    TextView updateBtn;
    int count = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        initToolbar();

        editLayout = findViewById(R.id.editLayout);
        oldPasswordLayout = findViewById(R.id.oldPasswordLayout);
        newPasswordLayout = findViewById(R.id.newPasswordLayout);
        confirmPasswordLayout = findViewById(R.id.confirmPasswordLayout);
        oldPasswordEt = findViewById(R.id.oldPasswordEt);
        newPasswordEt = findViewById(R.id.newPasswordEt);
        confirmPasswordEt = findViewById(R.id.confirmPasswordEt);
        updateBtn = findViewById(R.id.updateBtn);

        changeEnabledStatus(false);

        editLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ++count;
                if(count % 2 == 0){
                    changeEnabledStatus(true);
                } else {
                    changeEnabledStatus(false);
                }
            }
        });

    }

    public void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Change Password");
        backIv = toolbar.findViewById(R.id.backIv);

        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void changeEnabledStatus(boolean status){
        oldPasswordEt.setEnabled(status);
        newPasswordEt.setEnabled(status);
        confirmPasswordEt.setEnabled(status);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

}
