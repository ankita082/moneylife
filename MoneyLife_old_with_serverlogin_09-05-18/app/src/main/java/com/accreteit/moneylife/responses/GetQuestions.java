package com.accreteit.moneylife.responses;

import com.accreteit.moneylife.models.Expert;
import com.accreteit.moneylife.models.Question;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 22-Mar-18.
 */

public class GetQuestions {
    @SerializedName("question")
    @Expose
    private List<Question> question = null;
    @SerializedName("code")
    @Expose
    private Integer code;

    public List<Question> getQuestionList() {
        return question;
    }

    public void getQuestionList(List<Question> question) {
        this.question = question;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
