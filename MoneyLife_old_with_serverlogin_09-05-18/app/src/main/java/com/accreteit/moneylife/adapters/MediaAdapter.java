package com.accreteit.moneylife.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.accreteit.moneylife.R;
import com.accreteit.moneylife.models.Media;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MediaAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Media> mediaArrayList;

    public MediaAdapter(Context context, ArrayList<Media> mediaArrayList) {
        this.context = context;
        this.mediaArrayList = mediaArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mediaArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mediaArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_media, parent, false);
        }
        TextView fileNameTv = convertView.findViewById(R.id.fileNameTv);
        Media media = mediaArrayList.get(position);
        String url = media.getPath();
        String fileName = url.substring(url.lastIndexOf('/') + 1, url.length());
        fileNameTv.setText(fileName);

        return convertView;
    }
}
