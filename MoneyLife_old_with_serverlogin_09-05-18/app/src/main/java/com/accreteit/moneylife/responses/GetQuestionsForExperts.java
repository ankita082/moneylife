package com.accreteit.moneylife.responses;

import com.accreteit.moneylife.models.Question;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 22-Mar-18.
 */

public class GetQuestionsForExperts {
    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("question")
    @Expose
    private List<Question> questionList = null;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<Question> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<Question> questionList) {
        this.questionList = questionList;
    }

}
