package com.accreteit.moneylife.responses;

import com.accreteit.moneylife.models.Category;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Common {
    @SerializedName("code")
    @Expose
    private Integer code; // This param is used for login and signup url which is made at moneylife server
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message = "";
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("UserId")
    @Expose
    private String userIdFromServer;
    @SerializedName("is_user")
    @Expose
    private Integer isUser;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getIsUser() {
        return isUser;
    }

    public void setIsUser(Integer isUser) {
        this.isUser = isUser;
    }

    public String getUserIdFromServer() {
        return userIdFromServer;
    }

    public void setUserIdFromServer(String userIdFromServer) {
        this.userIdFromServer = userIdFromServer;
    }
}
