package com.accreteit.moneylife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.accreteit.moneylife.responses.Common;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.accreteit.moneylife.RetroApiClient.okHttpClient;

public class RegisterActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    EditText nameEt, emailEt, passwordEt, contactEt;
    TextInputLayout nameInput, emailInput, passwordInput, contactInput;
    TextView registerBtn, fbBtn, gPlusBtn, loginTv;
    LinearLayout fbLayout, gPlusLayout;
    String fbId = "", name = "", email = "", password = "", contact = "", deviceType = "0", token = "", registerType = "";
    private static final int RC_SIGN_IN = 100;
    CallbackManager callbackManager;
    GoogleApiClient mGoogleApiClient;
    GoogleSignInOptions gso;
    ProgressDialog pd;
    Boolean isValid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        nameInput = findViewById(R.id.nameInput);
        emailInput = findViewById(R.id.emailInput);
        passwordInput = findViewById(R.id.passwordInput);
        nameEt = findViewById(R.id.nameEt);
        emailEt = findViewById(R.id.emailEt);
        passwordEt = findViewById(R.id.passwordEt);
        fbLayout = findViewById(R.id.fbLayout);
        gPlusLayout = findViewById(R.id.gPlusLayout);
        registerBtn = findViewById(R.id.registerBtn);
        fbBtn = findViewById(R.id.fbBtn);
        gPlusBtn = findViewById(R.id.gPlusBtn);
        loginTv = findViewById(R.id.loginTv);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e("In RegisterActivity", "Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("In RegisterActivity", "Excp : " + e.getMessage());
        } catch (Exception e) {
            Log.e("In RegisterActivity", "Excp : " + e.getMessage());
        }

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Log.e("In RegisterActivity", "In OnSuccess");

                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    fbId = response.getJSONObject().getString("id");
                                    name = response.getJSONObject().getString("name");
                                    email = response.getJSONObject().getString("email");
                                    registerApiCall("1");
                                } catch (JSONException je) {
                                    Log.e("In RegisterActivity", "Graph JSON Excp : " + je.getMessage());
                                }
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.e("In RegisterActivity", "In OnCancel");
                        Toast.makeText(RegisterActivity.this, "Login Cancelled!", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.e("In RegisterActivity", "In OnError");
                        Toast.makeText(RegisterActivity.this, Config.FailureMsg, Toast.LENGTH_LONG).show();
                    }
                });

//        Values for registerType --> 0 -> For normal app registration, 1 -> For Fb and 2 -> For Google

        if (Config.getSharedPreferences(RegisterActivity.this, "token") != null) {
            token = Config.getSharedPreferences(RegisterActivity.this, "token");
        } else {
            token = FirebaseInstanceId.getInstance().getToken();
        }
        Config.saveSharedPreferences(RegisterActivity.this, "token", token);

        loginTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        fbLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerType = "1";
                LoginManager.getInstance().logInWithReadPermissions(RegisterActivity.this,
                        Arrays.asList("email", "public_profile"));
            }
        });

        gPlusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerType = "2";

                gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .requestProfile()
                        .requestId()
                        .build();

                if (mGoogleApiClient != null) {
                    Log.e("In RegisterActivity", "In IF");
                    OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
                    if (opr.isDone()) {
                        Log.e("In RegisterActivity", "In Inner IF");
                        // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
                        // and the GoogleSignInResult will be available instantly.
                        Log.e("In OnStart", "Got cached sign-in");
                        GoogleSignInResult result = opr.get();
                        handleSignInResult(result);
//                        Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                    } else {
                        Log.e("In RegisterActivity", "In Inner Else");
                        // If the user has not previously signed in on this device or the sign-in has expired,
                        // this asynchronous branch will attempt to sign in the user silently.  Cross-device
                        // single sign-on will occur in this branch.
                        /*pd = new ProgressDialog(RegisterActivity.this);
                        pd.setCancelable(false);
                        pd.setMessage("Please wait...");
                        pd.show();
                        opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                            @Override
                            public void onResult(GoogleSignInResult googleSignInResult) {
                                pd.dismiss();
                                handleSignInResult(googleSignInResult);
                            }
                        });*/
                        mGoogleApiClient = new GoogleApiClient.Builder(RegisterActivity.this)
//                            .enableAutoManage(RegisterActivity.this, RegisterActivity.this)
                                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                                .build();

                        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                        startActivityForResult(signInIntent, RC_SIGN_IN);
                    }
                } else {
                    Log.e("In RegisterActivity", "In Else");

                    mGoogleApiClient = new GoogleApiClient.Builder(RegisterActivity.this)
//                            .enableAutoManage(RegisterActivity.this, RegisterActivity.this)
                            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                            .build();

                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                }

            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isValid = true;
                name = nameEt.getText().toString();
                email = emailEt.getText().toString();
                password = passwordEt.getText().toString();
                nameInput.setErrorEnabled(false);
                emailInput.setErrorEnabled(false);
                passwordInput.setErrorEnabled(false);
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                registerType = "0";

                if (name.equalsIgnoreCase("")) {
                    isValid = false;
                    nameInput.setErrorEnabled(true);
                    nameInput.setError("Must enter name");
                }
                if (email.equalsIgnoreCase("")) {
                    isValid = false;
                    emailInput.setErrorEnabled(true);
                    emailInput.setError("Must enter email address");
                } else {
                    if (!email.matches(emailPattern)) {
                        isValid = false;
                        emailInput.setErrorEnabled(true);
                        emailInput.setError("Must enter valid email id");
                    }
                }
                if (password.equalsIgnoreCase("")) {
                    isValid = false;
                    passwordInput.setErrorEnabled(true);
                    passwordInput.setError("Must enter password");
                }
                if (isValid)
                    registerApiCall("0");

            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
//            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            handleSignInResult(task);
//            Task<GoogleSignInAccount> task = GoogleSignInApi.getSignedInAccountFromIntent(data);
//            handleSignInResult(task);

        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("In RegisterActivity", "In OnConnFailed" + connectionResult);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.e("In RegisterActivity", "In handleSignInResult : " + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            Log.e("In HandleSignIn", "Success : " + result.isSuccess());
            GoogleSignInAccount acct = result.getSignInAccount();
            /*String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();
            String id = acct.getId();*/

            Log.e("In HandleSignIn", "Name: " + acct.getDisplayName() + ", email: " + acct.getEmail()
                    + ", Image: " + acct.getPhotoUrl() + " n Id: " + acct.getId());
            nameEt.setText(acct.getDisplayName());
            emailEt.setText(acct.getEmail());

        } else {
            // Signed out, show unauthenticated UI.
            Log.e("In HandleSignIn", "In Failure");
        }
    }

    public void registerApiCall(String registerType) {
        if (Config.isConnectedToInternet(RegisterActivity.this)) {
            pd = new ProgressDialog(RegisterActivity.this);
            pd.setTitle("Please wait");
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();

            Log.e("In RegisterActivity", "Params : Name -> " + name + " Email -> " + email
                    + " Pwd -> " + password + " DeviceType -> " + deviceType
                    + " n RegisterType -> " + registerType);
            Retrofit retro = new Retrofit.Builder()
                    .baseUrl(Config.SignUpnLoginUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            RetroApiInterface apiInterface = retro.create(RetroApiInterface.class);
            Call<Common> call = apiInterface.register(name, email, password, deviceType, registerType);
            call.enqueue(new Callback<Common>() {
                @Override
                public void onResponse(Call<Common> call, Response<Common> response) {
                    pd.dismiss();
                    Common c = response.body();
                    Log.e("In RegisterActivity", "Response : " + new Gson().toJson(c));
                    if (c.getCode() == 100) {
//                        Config.saveSharedPreferences(RegisterActivity.this, "userId", c.getUserId());
                        // As only user can register, user type will always be 1
                        Config.saveSharedPreferences(RegisterActivity.this, "userType", "1");
                        final DialogPlus dialog = DialogPlus.newDialog(RegisterActivity.this)
                                .setContentHolder(new ViewHolder(R.layout.dialog_ok_layout))
                                .setContentHeight(LinearLayout.LayoutParams.WRAP_CONTENT)
                                .setGravity(Gravity.CENTER)
                                .create();
                        dialog.show();

                        View errorView = dialog.getHolderView();
                        TextView titleTv = (TextView) errorView.findViewById(R.id.titleTv);
                        TextView messageTv = (TextView) errorView.findViewById(R.id.messageTv);
                        Button okBtn = (Button) errorView.findViewById(R.id.okBtn);
                        titleTv.setVisibility(View.GONE);
                        messageTv.setText(c.getMessage());
//                        messageTv.setText("Please verify your account as it is not yet verified. After that you need to login to proceed further");
                        okBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                finish();
                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });

                    } else {
                        Config.showDialog(RegisterActivity.this, c.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<Common> call, Throwable t) {
                    pd.dismiss();
                    Log.e("In RegisterActivity", "OnFailure Excp : " + t.getMessage());
                    Config.showDialog(RegisterActivity.this, Config.FailureMsg);
                }
            });
        } else {
            Config.showAlertForInternet(RegisterActivity.this);
        }
    }

}
