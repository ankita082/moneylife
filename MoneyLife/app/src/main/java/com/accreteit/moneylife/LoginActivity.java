package com.accreteit.moneylife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.accreteit.moneylife.responses.Common;
import com.accreteit.moneylife.responses.CommonUsingServer;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.accreteit.moneylife.RetroApiClient.okHttpClient;

public class LoginActivity extends AppCompatActivity {

    EditText emailEt, passwordEt;
    TextInputLayout emailInput, passwordInput;
    TextView loginBtn, fbBtn, gPlusBtn, forgotPwdTv, registerTv;
    String email = "", password = "", deviceType = "0", token = "";
    ProgressDialog pd;
    Boolean isValid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailInput = findViewById(R.id.emailInput);
        passwordInput = findViewById(R.id.passwordInput);
        emailEt = findViewById(R.id.emailEt);
        passwordEt = findViewById(R.id.passwordEt);
        loginBtn = findViewById(R.id.loginBtn);
        fbBtn = findViewById(R.id.fbBtn);
        gPlusBtn = findViewById(R.id.gPlusBtn);
        forgotPwdTv = findViewById(R.id.forgotPwdTv);
        registerTv = findViewById(R.id.registerTv);

        if (Config.getSharedPreferences(LoginActivity.this, "token") != null) {
            token = Config.getSharedPreferences(LoginActivity.this, "token");
        } else {
            token = FirebaseInstanceId.getInstance().getToken();
        }
        Config.saveSharedPreferences(LoginActivity.this, "token", token);

          // Custom Data
        emailEt.setText("shashankbansal21@gmail.com");
        passwordEt.setText("123456");

        registerTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isValid = true;
                email = emailEt.getText().toString();
                password = passwordEt.getText().toString();
                emailInput.setErrorEnabled(false);
                passwordInput.setErrorEnabled(false);
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if (email.equalsIgnoreCase("")) {
                    isValid = false;
                    emailInput.setErrorEnabled(true);
                    emailInput.setError("Must enter email address");
                } else {
                    if (!email.matches(emailPattern)) {
                        isValid = false;
                        emailInput.setErrorEnabled(true);
                        emailInput.setError("Must enter valid email id");
                    }
                }
                if (password.equalsIgnoreCase("")) {
                    isValid = false;
                    passwordInput.setErrorEnabled(true);
                    passwordInput.setError("Must enter password");
                }

                if (isValid) {
                    if (Config.isConnectedToInternet(LoginActivity.this)) {
                        pd = new ProgressDialog(LoginActivity.this);
                        pd.setTitle("Please wait");
                        pd.setMessage("Loading..");
                        pd.setCancelable(false);
                        pd.show();

                        Log.e("In LoginActivity", "Params : Email -> " + email + " Pwd -> " + password
                                + " DeviceType -> " + deviceType + " n DeviceToken -> " + token);
                        /*Retrofit retro = new Retrofit.Builder()
                                .baseUrl(Config.SignUpnLoginUrl)
                                .addConverterFactory(GsonConverterFactory.create())
                                .client(okHttpClient)
                                .build();
                        RetroApiInterface apiInterface = retro.create(RetroApiInterface.class);
                        Call<CommonUsingServer> call = apiInterface.loginUsingServer(email, password);
                        call.enqueue(new Callback<CommonUsingServer>() {
                            @Override
                            public void onResponse(Call<CommonUsingServer> call, Response<CommonUsingServer> response) {
//                                pd.dismiss();
                                CommonUsingServer c = response.body();
                                Log.e("In LoginActivity", "Response : " + new Gson().toJson(c));
                                if (c != null && c.getCode() == 100) {
                                    pd.dismiss();
                                    Log.e("In LoginActivity", "In IF Actual Server");
                                    // Custom Data
                                    Config.saveSharedPreferences(LoginActivity.this, "userIdFromServer", c.getUserIdFromServer());
                                    Config.saveSharedPreferences(LoginActivity.this, "userId", "1");
                                    Config.saveSharedPreferences(LoginActivity.this, "userType", "1");
                                    finish();
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                } else {*/
                                    Log.e("In LoginActivity", "In Else Db");
                                    RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
                                    Call<Common> call1 = apiInterface.loginUsingDb(email, password, token, "0");
                                    call1.enqueue(new Callback<Common>() {
                                        @Override
                                        public void onResponse(Call<Common> call, Response<Common> response) {
                                            pd.dismiss();
                                            Common c = response.body();
                                            Config.saveSharedPreferences(LoginActivity.this, "userId", c.getUserId());
                                            Config.saveSharedPreferences(LoginActivity.this, "userType", c.getIsUser() + "");
                                            finish();
                                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }

                                        @Override
                                        public void onFailure(Call<Common> call, Throwable t) {
                                            pd.dismiss();
                                            Config.showDialog(LoginActivity.this, Config.FailureMsg);
                                        }
                                    });

                                /*}
                            }

                            @Override
                            public void onFailure(Call<CommonUsingServer> call, Throwable t) {
                                pd.dismiss();
                                Log.e("In LoginActivity", "OnFailure Excp : " + t.getMessage());
                                Config.showDialog(LoginActivity.this, Config.FailureMsg);
                            }
                        });*/
                    } else {
                        Config.showAlertForInternet(LoginActivity.this);
                    }
                }

            }
        });

    }
}
