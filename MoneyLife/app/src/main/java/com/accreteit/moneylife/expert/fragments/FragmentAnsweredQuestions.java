package com.accreteit.moneylife.expert.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.accreteit.moneylife.Config;
import com.accreteit.moneylife.MainActivity;
import com.accreteit.moneylife.R;
import com.accreteit.moneylife.RetroApiClient;
import com.accreteit.moneylife.RetroApiInterface;
import com.accreteit.moneylife.adapters.QuestionsAdapter;
import com.accreteit.moneylife.responses.GetQuestions;
import com.accreteit.moneylife.responses.GetQuestionsForExperts;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentAnsweredQuestions extends Fragment {

    TextView noDataTv;
    RecyclerView recyclerView;
    QuestionsAdapter adapter;
    ProgressDialog pd;
//    String questionJson = "{ \"code\": 100, \"query\": [ { \"question_id\": 1, \"question\": \"Ask Question Here\", \"question_image\": \"https://picsum.photos/458/354/?image=882\", \"expert_name\": \"Misthy Shah\", \"comments_number\": \"3\", \"is_answered\": \"1\" }, { \"question_id\": 2, \"question\": \"Ask Question Here\", \"question_image\": \"https://picsum.photos/458/354\", \"expert_name\": \"Gunjan Mehta\", \"comments_number\": \"9\", \"is_answered\": \"1\" }, { \"question_id\": 3, \"question\": \"Ask Question Here\", \"question_image\": \"https://picsum.photos/1263/285/?image=933\", \"expert_name\": \"Dev Dixit\", \"comments_number\": \"15\", \"is_answered\": \"1\" }, { \"question_id\": 4, \"question\": \"Ask Question Here\", \"question_image\": \"https://picsum.photos/458/354?gravity=east\", \"expert_name\": \"Somil Vyas\", \"comments_number\": \"22\", \"is_answered\": \"1\" } ] }";
//    GetQuestions q;

    public FragmentAnsweredQuestions() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_answered_questions, container, false);
        ((MainActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        setHasOptionsMenu(true);

        noDataTv = rootView.findViewById(R.id.noDataTv);
        recyclerView = rootView.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

//        For static data
//        q = new Gson().fromJson(questionJson, GetQuestions.class);
//        if (q.getQuestionList().size() > 0) {
//            adapter = new QuestionsAdapter(getActivity(), q.getQuestionList());
//            recyclerView.setAdapter(adapter);
//        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(Config.isConnectedToInternet(getActivity())){
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.show();

            String userId = Config.getSharedPreferences(getActivity(), "userId");
            RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
            Call<GetQuestionsForExperts> call = apiInterface.getAnsweredQuestions(userId);
            call.enqueue(new Callback<GetQuestionsForExperts>() {
                @Override
                public void onResponse(Call<GetQuestionsForExperts> call, Response<GetQuestionsForExperts> response) {
                    pd.dismiss();
                    GetQuestionsForExperts qe = response.body();
                    if(qe.getQuestionList().size() > 0){
                        noDataTv.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        adapter = new QuestionsAdapter(getActivity(), qe.getQuestionList());
                        recyclerView.setAdapter(adapter);
                    } else {
                        noDataTv.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<GetQuestionsForExperts> call, Throwable t) {
                    pd.dismiss();
                    Log.e("In FrgmtAnsweredQues", "OnFailure Excp : " + t.getMessage());
                }
            });
        } else {
            Config.showAlertForInternet(getActivity());
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        getActivity().onBackPressed();
        return true;
    }

}
