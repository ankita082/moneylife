package com.accreteit.moneylife;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyProfile extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolbarTitle;
    ImageView backIv;
    LinearLayout editLayout, changePictureLayout;
    TextInputLayout nameLayout, emailLayout, passwordLayout, contactLayout;
    EditText nameEt, emailEt, passwordEt, contactEt;
    TextView updateBtn;
    ImageView imageIv;
    int count = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        initToolbar();

        editLayout = findViewById(R.id.editLayout);
        changePictureLayout = findViewById(R.id.changePictureLayout);
        nameLayout = findViewById(R.id.nameLayout);
        emailLayout = findViewById(R.id.emailLayout);
        passwordLayout = findViewById(R.id.passwordLayout);
        contactLayout = findViewById(R.id.contactLayout);
        nameEt = findViewById(R.id.nameEt);
        emailEt = findViewById(R.id.emailEt);
        passwordEt = findViewById(R.id.passwordEt);
        contactEt = findViewById(R.id.contactEt);
        updateBtn = findViewById(R.id.updateBtn);
        imageIv = findViewById(R.id.imageIv);

        changeEnabledStatus(false);

        editLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ++count;
                if(count % 2 == 0){
                    changeEnabledStatus(true);
                } else {
                    changeEnabledStatus(false);
                }
            }
        });

    }

    public void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("My Profile");
        backIv = toolbar.findViewById(R.id.backIv);

        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void changeEnabledStatus(boolean status){
        nameEt.setEnabled(status);
        emailEt.setEnabled(status);
        passwordEt.setEnabled(status);
        contactEt.setEnabled(status);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

}
