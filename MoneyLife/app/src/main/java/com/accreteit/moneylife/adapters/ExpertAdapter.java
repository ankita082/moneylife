package com.accreteit.moneylife.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.accreteit.moneylife.R;
import com.accreteit.moneylife.expert.activity.ExpertDetail;
import com.accreteit.moneylife.models.Expert;
import com.accreteit.moneylife.models.Notification;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;


public class ExpertAdapter extends RecyclerView.Adapter<ExpertAdapter.MyViewHolder> {
    List<Expert> data;
    private LayoutInflater inflater;
    private Context context;

    public ExpertAdapter(Context context, List<Expert> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_expert_list, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                Intent intent = new Intent(context, ExpertDetail.class);
                intent.putExtra("expertId", data.get(position).getId());
                context.startActivity(intent);
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Expert e = data.get(position);
        Glide.with(context)
                .load(e.getImage())
                .apply(new RequestOptions().centerCrop()
                        .placeholder(R.drawable.image_loader)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true))
                .into(holder.imageIv);
        holder.nameTv.setText(e.getName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageIv;
        TextView nameTv;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageIv = (ImageView) itemView.findViewById(R.id.imageIv);
            nameTv = (TextView) itemView.findViewById(R.id.nameTv);
        }
    }
}
