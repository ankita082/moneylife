package com.accreteit.moneylife.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonUsingServer {
    @SerializedName("code")
    @Expose
    private Integer code; // This param is used for login and signup url which is made at moneylife server
    @SerializedName("message")
    @Expose
    private String message = "";
    @SerializedName("UserId")
    @Expose
    private String userIdFromServer;
    @SerializedName("isPremiem")
    @Expose
    private Integer isPremium;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserIdFromServer() {
        return userIdFromServer;
    }

    public void setUserIdFromServer(String userIdFromServer) {
        this.userIdFromServer = userIdFromServer;
    }

    public Integer getIsPremium() {
        return isPremium;
    }

    public void setIsPremium(Integer isPremium) {
        this.isPremium = isPremium;
    }
}
