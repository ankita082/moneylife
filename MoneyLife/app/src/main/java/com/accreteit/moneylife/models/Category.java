package com.accreteit.moneylife.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("category_name")
    @Expose
    private String categoryTitle;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("total_experts")
    @Expose
    private String expertsNumber;


    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getExpertsNumber() {
        return expertsNumber;
    }

    public void setExpertsNumber(String expertsNumber) {
        this.expertsNumber = expertsNumber;
    }
}
