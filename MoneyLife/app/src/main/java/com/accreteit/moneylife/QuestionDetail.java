package com.accreteit.moneylife;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.accreteit.moneylife.adapters.CommentAdapter;
import com.accreteit.moneylife.adapters.MediaAdapter;
import com.accreteit.moneylife.models.Media;
import com.accreteit.moneylife.models.Question;
import com.accreteit.moneylife.player.FullscreenVideoLayout;
import com.accreteit.moneylife.responses.AddComment;
import com.accreteit.moneylife.responses.Common;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionDetail extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolbarTitle;
    ImageView backIv;
    LinearLayout answerLayout, replyLayout, uploadFilesLayout, recordVideoLayout, uploadAttachmentsLayout,
            fileLayout, commentLayout;
    RelativeLayout expertVideoLayout, videoLayout ;
    CommentAdapter adapter;
    MediaAdapter mediaAdapter;
    MediaAdapter answerMediaAdapter;
    ProgressDialog pd;
    ImageView imageIv, imageIv1;
    TextView questionTv, userNameTv, questionDescriptionTv, dateTimeTv, readMoreTv, watchQuestionTv, queryMediaListTv,
            expertNameTv, answerTv, totalCommentsTv, attachmentsTv, submitTv, sendBtn;
    EditText titleEt, replyEt, commentEt;
    String isUser = "", videoUrl = "", replyVideoUrl = "", fromBtnClick = "", title = "", reply = "";
    String questionId = "";
    Question question;
    int isReadMore = 0;
    FullscreenVideoLayout expertVideoView, replyVideoView;
    RecyclerView commentsRv;
    ArrayList<Media> queryMediaList = new ArrayList<Media>();
    ArrayList<Media> answerMediaList = new ArrayList<>();
    static int i = 0;
    String[] mimeTypes =
            {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                    "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                    "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                    "text/plain",
                    "application/pdf",
                    "application/zip"};
    HashMap<Integer, String> urlList = new HashMap<Integer, String>();
    MultipartBody.Part video;
    ArrayList<MultipartBody.Part> attachmentList = new ArrayList<>();
    LayoutInflater attachmentInflater;
    static final int PERMISSION_REQ_CODE = 100;
    static final int REQUEST_VIDEO_CAPTURE = 1;
    static final int PICKFILE_REQUEST_CODE = 2;
    static final int PICKFILE_REQUEST_VIDEO = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_detail);

        initToolbar();

        isUser = Config.getSharedPreferences(QuestionDetail.this, "userType");
        commentLayout = findViewById(R.id.commentLayout);
        commentsRv = findViewById(R.id.commentsRv);
        commentsRv.setLayoutManager(new LinearLayoutManager(QuestionDetail.this));
        answerLayout = findViewById(R.id.answerLayout);
        imageIv = findViewById(R.id.imageIv);
        questionTv = findViewById(R.id.questionTv);
        userNameTv = findViewById(R.id.userNameTv);
        questionDescriptionTv = findViewById(R.id.questionDescriptionTv);
        dateTimeTv = findViewById(R.id.dateTimeTv);
        readMoreTv = findViewById(R.id.readMoreTv);
        watchQuestionTv = findViewById(R.id.watchQuestionTv);
        queryMediaListTv = findViewById(R.id.queryMediaListTv);
        imageIv1 = findViewById(R.id.imageIv1);
        expertNameTv = findViewById(R.id.expertNameTv);
        answerTv = findViewById(R.id.answerTv);
        attachmentsTv = findViewById(R.id.attachmentsTv);
        totalCommentsTv = findViewById(R.id.totalCommentsTv);
        expertVideoView = findViewById(R.id.expertVideoView);
        replyVideoView = findViewById(R.id.replyVideoView);
        replyLayout = findViewById(R.id.replyLayout);
        titleEt = findViewById(R.id.titleEt);
        replyEt = findViewById(R.id.replyEt);
        videoLayout = findViewById(R.id.videoLayout);
        expertVideoLayout = findViewById(R.id.expertVideoLayout);
        uploadFilesLayout = findViewById(R.id.uploadFilesLayout);
        recordVideoLayout = findViewById(R.id.recordVideoLayout);
        uploadAttachmentsLayout = findViewById(R.id.uploadAttachmentsLayout);
        fileLayout = findViewById(R.id.fileLayout);
        submitTv = findViewById(R.id.submitTv);
        commentEt = findViewById(R.id.commentEt);
        sendBtn = findViewById(R.id.sendBtn);
        expertVideoView.setActivity(this);
        replyVideoView.setActivity(this);

        attachmentInflater = LayoutInflater.from(QuestionDetail.this);
//        MediaController mediaController = new MediaController(QuestionDetail.this);
//        mediaController.setAnchorView(expertVideoView);
//        mediaController.setOverScrollMode(View.OVER_SCROLL_NEVER);
//        expertVideoView.setMediaController(mediaController);

        questionId = getIntent().getStringExtra("questionId");
        Log.e("questionId", "ID : " + questionId);
//        if (isUser.equals("1")) {
//            commentLayout.setVisibility(View.GONE);
//        } else if (isUser.equals("0")) {
//            commentLayout.setVisibility(View.VISIBLE);
//        }

        // Note :- An api call to get question details. Function is defined to refresh the layout as and when
        // query is either replied or commented
        getQuestionDetails();

        readMoreTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isReadMore == 0) {
                    questionDescriptionTv.setText(question.getQuestionDescription());
                    readMoreTv.setText("Read Less");
                    isReadMore = 1;
                } else {
                    questionDescriptionTv.setText(question.getQuestionDescription().substring(0, 350));
                    readMoreTv.setText("Read More");
                    isReadMore = 0;
                }
            }
        });

        queryMediaListTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogPlus dialog = DialogPlus.newDialog(QuestionDetail.this)
                        .setAdapter(mediaAdapter)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                Intent intent = new Intent(QuestionDetail.this, WebViewActivity.class);
                                intent.putExtra("url", queryMediaList.get(position).getPath());
                                startActivity(intent);
                            }
                        })
                        .setExpanded(true)  // This will enable the expand feature, (similar to android L share dialog)
                        .create();
                dialog.show();
            }
        });

        attachmentsTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogPlus dialog = DialogPlus.newDialog(QuestionDetail.this)
                        .setAdapter(answerMediaAdapter)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                                Intent intent = new Intent(QuestionDetail.this, WebViewActivity.class);
                                intent.putExtra("url", answerMediaList.get(position).getPath());
                                startActivity(intent);
                            }
                        })
                        .setExpanded(true)  // This will enable the expand feature, (similar to android L share dialog)
                        .create();
                dialog.show();
            }
        });

        watchQuestionTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuestionDetail.this, ShowVideoActivity.class);
                intent.putExtra("url", videoUrl);
                startActivity(intent);
            }
        });

        // Note :- Below is the code for replying the query asked by user
        recordVideoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromBtnClick = "Record";
                if (checkPermissions()) {
                    if (replyVideoUrl.equalsIgnoreCase("")) {
                        dispatchTakeVideoIntent();
                    } else {
                        AlertDialog dialog = new AlertDialog.Builder(QuestionDetail.this)
                                .setTitle("Confirmation")
                                .setMessage("By clicking continue, uploaded video will be replaced with the new video.")
                                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        replyVideoView.reset();
                                        dispatchTakeVideoIntent();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .setCancelable(true)
                                .create();
                        dialog.show();
                    }
                }
            }
        });

        uploadFilesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromBtnClick = "Upload";
                if (checkPermissions()) {
                    if (replyVideoUrl == null || replyVideoUrl.equalsIgnoreCase("")) {
                        dispatchSelectVideoIntent();
                    } else {
                        AlertDialog dialog = new AlertDialog.Builder(QuestionDetail.this)
                                .setTitle("Confirmation")
                                .setMessage("By clicking continue, uploaded video will be replaced with the new video.")
                                .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dispatchSelectVideoIntent();
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .setCancelable(true)
                                .create();
                        dialog.show();
                    }
                }
            }
        });

        uploadAttachmentsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromBtnClick = "Attach";
                if (checkPermissions()) {
                    dispatchTakeFilesIntent();
                }
            }
        });

        submitTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isValid = true;
                String message = "";
                title = titleEt.getText().toString();
                reply = replyEt.getText().toString();

                if (title.equalsIgnoreCase("")) {
                    isValid = false;
                    message = "Must enter short description for answer";
                }
                if (reply.equalsIgnoreCase("")) {
                    isValid = false;
                    message = "Must enter long description for answer";
                }

                if (!replyVideoUrl.equalsIgnoreCase("")) {
                    video = prepareFilePart("answer_video", Uri.parse(replyVideoUrl), "1");
                }
                if (urlList.size() > 0) {
                    int x = 1;
                    for (Integer key : urlList.keySet()) {
                        String url = urlList.get(key);
                        attachmentList.add(prepareFilePart("attachment_" + x, Uri.parse(url), "0"));
                        x++;
                    }
                }

                if (isValid) {
                    if (Config.isConnectedToInternet(QuestionDetail.this)) {
                        pd = new ProgressDialog(QuestionDetail.this);
                        pd.setTitle("Please wait");
                        pd.setMessage("Loading..");
                        pd.setCancelable(false);
                        pd.show();

                        RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
                        Call<Common> call = null;
                        if (video == null && attachmentList.size() == 0) {
                            Log.e("In QuestionDetail", "In If : Both video and attachments are not selected");
                            call = apiInterface.answerQueryWithoutMultipart(questionId,
                                    Config.getSharedPreferences(QuestionDetail.this, "userId"), title, reply);
                        } else {
                            Log.e("In QuestionDetail", "In Else : Either of video and attachments are selected");
                            call = apiInterface.answerQuery(questionId,
                                    Config.getSharedPreferences(QuestionDetail.this, "userId"), title, reply,
                                    video, attachmentList, "" + attachmentList.size());
                        }

                        call.enqueue(new Callback<Common>() {
                            @Override
                            public void onResponse(Call<Common> call, Response<Common> response) {
                                pd.dismiss();
                                Common ac = response.body();
                                Log.e("In QuestionDetail", "Response : " + new Gson().toJson(ac));
                                if (ac.getSuccess() == 1) {
//                                    Config.showDialog(QuestionDetail.this, "Files are successfully uploaded");
                                    final DialogPlus dialog = DialogPlus.newDialog(QuestionDetail.this)
                                            .setContentHolder(new ViewHolder(R.layout.dialog_ok_layout))
                                            .setContentHeight(LinearLayout.LayoutParams.WRAP_CONTENT)
                                            .setGravity(Gravity.CENTER)
                                            .create();
                                    dialog.show();

                                    View errorView = dialog.getHolderView();
                                    TextView titleTv = (TextView) errorView.findViewById(R.id.titleTv);
                                    TextView messageTv = (TextView) errorView.findViewById(R.id.messageTv);
                                    Button okBtn = (Button) errorView.findViewById(R.id.okBtn);
                                    titleTv.setVisibility(View.GONE);
                                    messageTv.setText(ac.getMessage());
                                    okBtn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                            getQuestionDetails();
                                        }
                                    });
                                } else {
                                    pd.dismiss();
                                    Config.showDialog(QuestionDetail.this, ac.getMessage());
                                }
                            }

                            @Override
                            public void onFailure(Call<Common> call, Throwable t) {
                                pd.dismiss();
                                Log.e("In QuestionDetail", "Excp :" + t.getMessage());
                                Config.showDialog(QuestionDetail.this, Config.FailureMsg);
                            }
                        });
                    } else {
                        Config.showAlertForInternet(QuestionDetail.this);
                    }
                } else {
                    Config.showDialog(QuestionDetail.this, message);
                }
            }
        });

        // Note :- Below is the code for adding comment
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = true;
                String message = "";

                if(commentEt.getText().toString().equalsIgnoreCase("")){
                    isValid = false;
                    message = "Must enter comment";
                }

                if(isValid){
                    Config.hideSoftKeyboard(QuestionDetail.this);
                    pd = new ProgressDialog(QuestionDetail.this);
                    pd.setMessage("Loading...");
                    pd.setCancelable(false);
                    pd.show();

                    Log.e("In QuestionDetail", "Params : UserId : "
                            + Config.getSharedPreferences(QuestionDetail.this, "userId") + ", UserType = "
                            + Config.getSharedPreferences(QuestionDetail.this, "userType") +  ", QuesId : " + questionId
                            + ", Comment : " + commentEt.getText().toString());
                    RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
                    Call<AddComment> call = apiInterface.addComment(Config.getSharedPreferences(QuestionDetail.this, "userId"),
                            Config.getSharedPreferences(QuestionDetail.this, "userType"), questionId,
                            commentEt.getText().toString());
                    call.enqueue(new Callback<AddComment>() {
                        @Override
                        public void onResponse(Call<AddComment> call, Response<AddComment> response) {
                            pd.dismiss();
                            AddComment ac = response.body();
                            if(ac.getSuccess() == 1){
                                commentEt.setText("");
                                if (ac.getComments().size() > 0) {
                                    totalCommentsTv.setText(ac.getComments().size() + " Comments");
                                    adapter = new CommentAdapter(QuestionDetail.this, ac.getComments());
                                    commentsRv.setAdapter(adapter);
                                } else {
                                    totalCommentsTv.setText("No Comments");
                                }
                                commentsRv.smoothScrollToPosition(ac.getComments().size() - 1);
                            } else {
                                Config.showDialog(QuestionDetail.this, ac.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<AddComment> call, Throwable t) {
                            pd.dismiss();
                            Log.e("In QuestionDetail", "OnFailure Excp : " + t.getMessage());
                            Config.showDialog(QuestionDetail.this, Config.FailureMsg);
                        }
                    });

                } else {
                    Config.showDialog(QuestionDetail.this, message);
                }
            }
        });
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri, String isVideo) {
        File file = new File(fileUri.getPath());
        RequestBody requestFile = null;
        if (isVideo.equals("0")) {
            if (file != null)
                requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        } else if (isVideo.equals("1")) {
            if (file != null)
                requestFile = RequestBody.create(MediaType.parse("video/*"), file);
        }

        if (requestFile != null) {
            // MultipartBody.Part is used to send also the actual file name
            return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
        } else {
            Log.e("Exception", "Exception : File is null");
        }
        return null;

    }

    public void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = toolbar.findViewById(R.id.toolbar_title);
        toolbarTitle.setText("Question Detail");
        backIv = toolbar.findViewById(R.id.backIv);

        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void getQuestionDetails() {
        if (Config.isConnectedToInternet(QuestionDetail.this)) {
            if (pd != null) {
                pd = new ProgressDialog(QuestionDetail.this);
                pd.setTitle("Please wait");
                pd.setMessage("Loading..");
                pd.setCancelable(false);
                pd.show();
            }

            RetroApiInterface apiInterface = RetroApiClient.getClient().create(RetroApiInterface.class);
            Call<Question> call = apiInterface.getQuestionDetails(questionId);
            call.enqueue(new Callback<Question>() {
                @Override
                public void onResponse(Call<Question> call, Response<Question> response) {
                    if (pd != null && pd.isShowing())
                        pd.dismiss();
                    question = response.body();

                    if (Config.getSharedPreferences(QuestionDetail.this, "userType").equals("1")) {
                        replyLayout.setVisibility(View.GONE);
                        commentLayout.setVisibility(View.GONE);

                    } else if (Config.getSharedPreferences(QuestionDetail.this, "userType").equals("2")) {
                        replyLayout.setVisibility(View.GONE);
                        commentLayout.setVisibility(View.VISIBLE);
                        if (Config.getSharedPreferences(QuestionDetail.this, "userId")
                                .equals(String.valueOf(question.getExpertId()))) {
                            videoLayout.setVisibility(View.GONE);
                            if (question.getIsAnswered() == 1) {
                                replyLayout.setVisibility(View.GONE);
                            } else {
                                replyLayout.setVisibility(View.VISIBLE);
                            }
                        }

                    }

                    Glide.with(QuestionDetail.this)
                            .load(question.getExpertImage())
                            .apply(new RequestOptions().circleCrop()
                                    .placeholder(R.drawable.image_loader)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true))
                            .into(imageIv);
                    questionTv.setText(question.getQuestionTitle());
                    if (question.getQuestionDescription().length() > 350) {
                        readMoreTv.setVisibility(View.VISIBLE);
                        questionDescriptionTv.setText(question.getQuestionDescription().substring(0, 350));
                    } else {
                        readMoreTv.setVisibility(View.GONE);
                        questionDescriptionTv.setText(question.getQuestionDescription());
                    }

                    userNameTv.setText(question.getUserName());
                    dateTimeTv.setText(question.getDateTime());
                    videoUrl = question.getQuestionVideo();
                    if (videoUrl.equalsIgnoreCase("")) {
                        watchQuestionTv.setVisibility(View.GONE);
                    }

                    queryMediaList = (ArrayList<Media>) question.getQueryMediaList();
                    answerMediaList = (ArrayList<Media>) question.getAnswerMediaList();
                    if (queryMediaList.size() == 0) {
                        queryMediaListTv.setVisibility(View.GONE);
                    } else {
                        mediaAdapter = new MediaAdapter(QuestionDetail.this, queryMediaList);
                    }
//                    question.setIsAnswered(1);
                    expertVideoLayout.setVisibility(View.GONE);
                    if (question.getIsAnswered() == 1) {
                        answerLayout.setVisibility(View.VISIBLE);
                        Glide.with(QuestionDetail.this)
                                .load(question.getExpertImage())
                                .apply(new RequestOptions().circleCrop()
                                        .placeholder(R.drawable.image_loader)
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true))
                                .into(imageIv1);
                        expertNameTv.setText(question.getExpertName());
                        answerTv.setText(question.getAnswer());
//                        question.setAnswerVideo("http://accreteit.com/moneylife_app/api/query_media/query_video_2.mp4");
                        if (!question.getAnswerVideo().equalsIgnoreCase("")) {
                            try {
                                expertVideoLayout.setVisibility(View.VISIBLE);
                                expertVideoView.setVideoURI(Uri.parse(question.getAnswerVideo()));
                                expertVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                    @Override
                                    public void onPrepared(MediaPlayer mp) {
                                        expertVideoView.seekTo(500);
                                    }
                                });

                            } catch (IOException e) {
                                expertVideoLayout.setVisibility(View.GONE);
                            }
                        } else
                            expertVideoLayout.setVisibility(View.GONE);
                    } else {
                        answerLayout.setVisibility(View.GONE);
                    }

                    answerMediaList = (ArrayList<Media>) question.getAnswerMediaList();
                    if (answerMediaList.size() == 0) {
                        attachmentsTv.setVisibility(View.GONE);
                    } else {
                        attachmentsTv.setText(answerMediaList.size() + " Files");
                        answerMediaAdapter = new MediaAdapter(QuestionDetail.this, answerMediaList);
                    }
                    if (question.getCommentsList().size() > 0) {
                        totalCommentsTv.setText(question.getCommentsList().size() + " Comments");
                        adapter = new CommentAdapter(QuestionDetail.this, question.getCommentsList());
                        commentsRv.setAdapter(adapter);
                    } else {
                        totalCommentsTv.setText("No Comments");
                    }
                    commentsRv.smoothScrollToPosition(0);

                }

                @Override
                public void onFailure(Call<Question> call, Throwable t) {
                    if (pd != null && pd.isShowing())
                        pd.dismiss();
                    Log.e("In QuestionDetail", "In OnFailure : " + t.getMessage());
                    Config.showDialog(QuestionDetail.this, Config.FailureMsg);
                }
            });
        } else {
            Config.showAlertForInternet(QuestionDetail.this);
        }

    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(QuestionDetail.this.getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    private void dispatchTakeFilesIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
//        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(intent, PICKFILE_REQUEST_CODE);
    }

    private void dispatchSelectVideoIntent() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("video/*");
        startActivityForResult(intent, PICKFILE_REQUEST_VIDEO);
    }

    public static String getFilePath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        try {
            if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
                Uri videoUri = intent.getData();
                replyVideoUrl = getFilePath(QuestionDetail.this, videoUri);
                Log.e("In OnActResult", "In IF VideoPath : " + videoUrl);
                videoLayout.setVisibility(View.VISIBLE);
//                try {
                replyVideoView.reset();
                replyVideoView.setVideoURI(videoUri);
//                } catch (IOException e) {
//                    Log.e("Video URL", "Video Exception : " + e.getMessage());
//                }
                replyVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        replyVideoView.seekTo(500);
                    }
                });
            } else if (requestCode == PICKFILE_REQUEST_CODE && resultCode == RESULT_OK) {
                Uri docUri = intent.getData();
                String url = getFilePath(QuestionDetail.this, docUri);
                Log.e("In OnActResult", "In Else IF ImagePath : " + url);
                if (urlList.containsValue(url)) {
                    Toast.makeText(QuestionDetail.this, "File already Selected", Toast.LENGTH_SHORT).show();
                } else {
                    View view = attachmentInflater.inflate(R.layout.row_uploaded_attachments, null);
                    TextView fileNameTv = view.findViewById(R.id.fileNameTv);
                    ImageView closeIv = view.findViewById(R.id.closeIv);
                    urlList.put(i, url);
                    closeIv.setTag(i);
                    i++;
                    fileNameTv.setText(docUri.getLastPathSegment());
                    closeIv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int position = (int) v.getTag();
                            urlList.remove(position);
                            fileLayout.removeView((View) v.getParent());
                        }
                    });
                    fileLayout.addView(view);
                }
            } else if (requestCode == PICKFILE_REQUEST_VIDEO && resultCode == RESULT_OK) {
                replyVideoUrl = getFilePath(QuestionDetail.this, intent.getData());
                Log.e("In OnActResult", "In Else IF1 VideoPath : " + videoUrl);
                videoLayout.setVisibility(View.VISIBLE);
//                try {
                replyVideoView.reset();
                replyVideoView.setVideoURI(intent.getData());
//                } catch (IOException e) {
//                    Log.e("Video URL", "Video Exception : " + e.getMessage());
//                }
                replyVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        replyVideoView.seekTo(500);
                    }
                });
            }
        } catch (Exception e) {
            Log.e("In OnActResult", "Exception : " + e.getMessage());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQ_CODE) {
            if (grantResults.length > 0) {
                Log.e("In QuestionDetail", "In OnRequest In IF");
                if (fromBtnClick.equals("Record")) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                            grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        Log.e("In QuestionDetail", "In Inner IF : " + fromBtnClick);
                        if (videoUrl == null || videoUrl.equalsIgnoreCase("")) {
                            dispatchTakeVideoIntent();
                        } else {
                            AlertDialog dialog = new AlertDialog.Builder(QuestionDetail.this)
                                    .setTitle("Confirmation")
                                    .setMessage("By clicking continue, uploaded video will be replaced with the new video.")
                                    .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            replyVideoView.reset();
                                            dispatchTakeVideoIntent();
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .setCancelable(true)
                                    .create();
                            dialog.show();
                        }
                    }
                } else if (fromBtnClick.equals("Upload") || fromBtnClick.equals("Attach")) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Log.e("In QuestionDetail", "In Inner IF : " + fromBtnClick);
                        if (fromBtnClick.equals("Upload")) {
                            if (videoUrl == null || videoUrl.equalsIgnoreCase("")) {
                                dispatchSelectVideoIntent();
                            } else {
                                AlertDialog dialog = new AlertDialog.Builder(QuestionDetail.this)
                                        .setTitle("Confirmation")
                                        .setMessage("By clicking continue, uploaded video will be replaced with the new video.")
                                        .setPositiveButton("Continue", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                replyVideoView.reset();
                                                dispatchSelectVideoIntent();
                                            }
                                        })
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .setCancelable(true)
                                        .create();
                                dialog.show();
                            }
                        } else {
                            dispatchTakeFilesIntent();
                        }
                    }
                }
            }
        }
    }

    public boolean checkPermissions() {
        if (fromBtnClick.equals("Record")) {
            if (ContextCompat.checkSelfPermission(QuestionDetail.this, android.Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(QuestionDetail.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                runtimePermissions();
                return false;
            }
        } else if (fromBtnClick.equals("Upload") || fromBtnClick.equals("Attach")) {
            if (ContextCompat.checkSelfPermission(QuestionDetail.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                runtimePermissions();
                return false;
            }
        } else {
            runtimePermissions();
            return false;
        }
    }

    public void runtimePermissions() {
        if (fromBtnClick.equals("Record"))
            requestPermissions(new String[]{android.Manifest.permission.CAMERA,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQ_CODE);
        else
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQ_CODE);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        onBackPressed();
//        return true;
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (replyVideoView != null && replyVideoView.isFullscreen()) {
            replyVideoView.setFullscreen(false);
        }
    }
}
