package com.accreteit.moneylife.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.accreteit.moneylife.QuestionsList;
import com.accreteit.moneylife.R;
import com.accreteit.moneylife.models.Category;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> implements Filterable{
    List<Category> data, filteredData;
    private LayoutInflater inflater;
    private Context context;
    SearchFilter searchfilter;

    public CategoryAdapter(Context context, List<Category> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
        this.filteredData = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_category_list, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                Intent intent = new Intent(context, QuestionsList.class);
                intent.putExtra("categoryId", data.get(position).getCategoryId() + "");
                intent.putExtra("categoryName", data.get(position).getCategoryTitle() + "");
                context.startActivity(intent);
            }
        });

        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Category ac = data.get(position);
        Glide.with(context)
                .load(ac.getCategoryImage())
                .apply(new RequestOptions().fitCenter()
                        .placeholder(R.drawable.image_loader)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true))
                .into(holder.bgIv);
        holder.categoryNameTv.setText(ac.getCategoryTitle());
        holder.expertNumTv.setText(ac.getExpertsNumber() + " Experts");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public Filter getFilter() {
        if(searchfilter == null)
            searchfilter = new SearchFilter();
        return searchfilter;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout mainLayout;
        ImageView bgIv;
        TextView categoryNameTv, expertNumTv;

        public MyViewHolder(View itemView) {
            super(itemView);
            mainLayout = (RelativeLayout) itemView.findViewById(R.id.mainLayout);
            bgIv = (ImageView) itemView.findViewById(R.id.bgIv);
            categoryNameTv = (TextView) itemView.findViewById(R.id.categoryNameTv);
            expertNumTv = (TextView) itemView.findViewById(R.id.expertNumTv);
        }
    }

    private class SearchFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = filteredData;
                results.count = filteredData.size();
            }
            else {
                // We perform filtering operation
                List<Category> nList = new ArrayList<Category>();

                for (Category c : filteredData) {
                    if (c.getCategoryTitle().toLowerCase()
                            .contains(constraint.toString().toLowerCase()))
                        nList.add(c);
                }

                results.values = nList;
                results.count = nList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            data = (List<Category>) results.values;
            notifyDataSetChanged();
        }
    }

}
