Note :- 'is_answered' field is mentioned whenever experts' requests are listed while
in other cases such as i) Questions listed by category and ii) Listed of questions posted by users
'is_answered' field is absent

{
  "code": 100,
  "question_list": [
    {
      "question_id": 1,
      "question": "Ask Question Here",
      "question_image": "https://picsum.photos/458/354/?image=882",
      "expert_name": "Misthy Shah",
      "comments_number": "3",
	  "is_answered": "0"
    },
    {
      "question_id": 2,
      "question": "Ask Question Here",
      "question_image": "https://picsum.photos/458/354",
      "expert_name": "Gunjan Mehta",
      "comments_number": "9",
	  "is_answered": "0"
    },
    {
      "question_id": 3,
      "question": "Ask Question Here",
      "question_image": "https://picsum.photos/1263/285/?image=933",
      "expert_name": "Dev Dixit",
      "comments_number": "15",
	  "is_answered": "0"
    },
    {
      "question_id": 4,
      "question": "Ask Question Here",
      "question_image": "https://picsum.photos/458/354?gravity=east",
      "expert_name": "Somil Vyas",
      "comments_number": "22",
	  "is_answered": "0"
    }
  ]
}